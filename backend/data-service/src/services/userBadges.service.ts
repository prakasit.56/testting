import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as UserBadgesModel from 'src/models/user-badges'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class UserBadgesService {
  constructor(private prisma: PrismaService) {}

  async create(data: any): Promise<UserBadgesModel.UserBadges | Error> {
    /**
     * Create user badge record
     */
    try {
      // Insert user badge record to database
      return await this.prisma.userBadges.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: UserBadgesModel.FindManyUserBadgesArgs,
    select: any,
  ): Promise<UserBadgesModel.UserBadges[] | Error> {
    /**
     * Get user badge records
     */
    try {
      // Get user badge records from database
      const result = await this.prisma.userBadges.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: UserBadgesModel.UpdateOneUserBadgesArgs,
    select: any,
  ): Promise<UserBadgesModel.UserBadges | Error> {
    /**
     * Update or create user badge record
     */
    try {
      // Update user badge record
      return await this.prisma.userBadges.update({ ...args, ...select })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: UserBadgesModel.DeleteOneUserBadgesArgs,
  ): Promise<UserBadgesModel.UserBadges | Error> {
    /**
     * Delete user badge record
     */
    try {
      // Delete user badge record
      return await this.prisma.userBadges.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
