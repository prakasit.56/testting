import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as BadgesModel from 'src/models/badges'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class BadgesService {
  constructor(private prisma: PrismaService) {}

  async create(data: any): Promise<BadgesModel.Badges | Error> {
    /**
     * Create badge records
     */
    try {
      // Insert Badges record to database
      return await this.prisma.badges.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: BadgesModel.FindManyBadgesArgs,
    select: any,
  ): Promise<BadgesModel.Badges[] | Error> {
    /**
     * Get badge records
     */
    try {
      // Get badge records from database
      const result = await this.prisma.badges.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: BadgesModel.UpdateOneBadgesArgs,
    select: any,
  ): Promise<BadgesModel.Badges | Error> {
    /**
     * Update or create badge record
     */
    try {
      // Update badge record
      return await this.prisma.badges.update({ ...args, ...select })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: BadgesModel.DeleteOneBadgesArgs,
  ): Promise<BadgesModel.Badges | Error> {
    /**
     * Delete badge record
     */
    try {
      // Delete User record
      return await this.prisma.badges.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
