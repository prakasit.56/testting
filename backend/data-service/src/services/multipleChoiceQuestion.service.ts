import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as MultipleChoiceQuestionModel from 'src/models/multiple-choice-question'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class MultipleChoiceQuestionService {
  constructor(private prisma: PrismaService) {}

  async create(
    data: any,
  ): Promise<MultipleChoiceQuestionModel.MultipleChoiceQuestion | Error> {
    /**
     * Create multiple choice question record
     */
    try {
      // Insert multiple choice question record to database
      return await this.prisma.multipleChoiceQuestion.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: MultipleChoiceQuestionModel.FindManyMultipleChoiceQuestionArgs,
    select: any,
  ): Promise<MultipleChoiceQuestionModel.MultipleChoiceQuestion[] | Error> {
    /**
     * Get multiple choice question records
     */
    try {
      // Get multiple choice question records from database
      const result = await this.prisma.multipleChoiceQuestion.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: MultipleChoiceQuestionModel.UpdateOneMultipleChoiceQuestionArgs,
    select: any,
  ): Promise<MultipleChoiceQuestionModel.MultipleChoiceQuestion | Error> {
    /**
     * Update or create multiple choice question record
     */
    try {
      // Update multiple choice question record
      return await this.prisma.multipleChoiceQuestion.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: MultipleChoiceQuestionModel.DeleteOneMultipleChoiceQuestionArgs,
  ): Promise<MultipleChoiceQuestionModel.MultipleChoiceQuestion | Error> {
    /**
     * Delete multiple choice question record
     */
    try {
      // Delete multiple choice question record
      return await this.prisma.multipleChoiceQuestion.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
