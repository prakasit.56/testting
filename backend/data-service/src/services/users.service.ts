import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
  UnauthorizedException,
  BadRequestException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as UsersModel from 'src/models/users'
import * as UserTokensModel from 'src/models/user-tokens'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'
import { AuthService } from 'src/authentication/auth.service'
import { UserTokensService } from 'src/services/userToken.service'
import { MinioClientService } from 'src/providers/storage/minio/minio.service'

@Injectable()
export class UsersService {
  constructor(
    private prisma: PrismaService,
    private authService: AuthService,
    private userTokensService: UserTokensService,
    private minioClientService: MinioClientService,
  ) {}

  async create(data: any): Promise<UsersModel.UsersWithoutPassword | Error> {
    /**
     * Create User record
     */
    try {
      // Set user role as USER
      const userRole = await this.prisma.userRoles.findFirst({
        where: {
          name: { equals: 'USER' },
        },
        select: {
          user_role_id: true,
        },
      })
      data.data.user_role.connect.user_role_id = userRole.user_role_id

      // Enscript password
      data.data.password = await this.authService.hash(data.data.password)
      // Insert User record to database
      return await this.prisma.users.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: UsersModel.FindManyUsersArgs,
    select: any,
    user: UsersModel.Users,
  ): Promise<UsersModel.UsersWithoutPassword[] | Error> {
    /**
     * Get User records
     */
    try {
      if (user.user_role.name !== UserRoleModel.Role.ADMIN)
        if (args.where)
          args.where = {
            ...args.where,
            user_id: { equals: user.user_id },
          }
        else
          args = {
            ...args,
            where: { user_id: { equals: user.user_id } },
          }

      // Get User records from database
      const result = await this.prisma.users.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async getOne(
    args: UsersModel.FindFirstUsersArgs,
    select: any,
  ): Promise<UsersModel.UsersWithoutPassword | Error> {
    /**
     * Get User records
     */
    try {
      // Get one Users record from database
      return await this.prisma.users.findFirst({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: UsersModel.UpdateOneUsersArgs,
    select: any,
    user: UsersModel.Users,
  ): Promise<UsersModel.UsersWithoutPassword | Error> {
    /**
     * Update or create Users record
     */
    try {
      if (user.user_role.name !== UserRoleModel.Role.ADMIN)
        if (args.where)
          args.where = {
            ...args.where,
            user_id: user.user_id,
          }
        else
          args = {
            ...args,
            where: { user_id: user.user_id },
          }

      // If user try to update another user
      if (user.user_id !== args.where.user_id)
        return new NotFoundException('User not found')

      // hash new password
      if (args.data.password)
        args.data.password.set = await this.authService.hash(
          args.data.password.set,
        )

      // Update User record
      return await this.prisma.users.update({ ...args, ...select })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: UsersModel.DeleteOneUsersArgs,
  ): Promise<UsersModel.UsersWithoutPassword | Error> {
    /**
     * Delete Users record
     */
    try {
      // Delete User record
      return await this.prisma.users.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async login(
    data: UsersModel.UsersLoginInput,
  ): Promise<UsersModel.UsersLoginOutput | Error> {
    /**
     * Login
     */
    try {
      // Find user by username
      const getResult: UsersModel.Users = await this.prisma.users.findFirst({
        where: {
          username: data.username,
        },
      })
      if (getResult === null) return new UnauthorizedException('User not found')

      const isMatch: boolean = await this.authService.compare(
        data.password,
        getResult.password,
      )

      if (!isMatch) return new UnauthorizedException('User not found')

      const result = getResult as UsersModel.UsersLoginOutput
      const tokens = await this.authService.generate(getResult)

      // Insert refreshToken into database
      const dataInsert: UserTokensModel.CreateOneUserTokensArgs = {
        data: {
          users: { connect: { user_id: result.user_id } },
          token: tokens.refresh_token,
        },
      }

      const resultInsert = await this.userTokensService.create(dataInsert)

      if (resultInsert instanceof Error)
        return new UnauthorizedException('Can not insert new refresh token')

      result.access_token = tokens.access_token
      result.refresh_token = tokens.refresh_token

      return result
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          throw new NotFoundException(Object.values(e.meta)[0])
      }
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async logout(data: UsersModel.UsersLogoutInput): Promise<boolean | Error> {
    /**
     * Logout
     */
    try {
      const argsDelete: UserTokensModel.DeleteManyUserTokensArgs = {
        where: {
          AND: [
            {
              token: { equals: data.refresh_token },
            },
            {
              user_id: { equals: data.user_id },
            },
          ],
        },
      }
      const resultDelete = await this.userTokensService.deleteMany(argsDelete)
      if (resultDelete instanceof Error)
        return new UnauthorizedException('Can not delete refresh token')

      return resultDelete > 0
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async refreshToken(
    data: UserTokensModel.UserRefreshTokensInput,
    // user: UserModel.User,
  ): Promise<UserTokensModel.UserRefreshTokensOutput | Error> {
    /**
     * refreshToken
     */
    try {
      // Check old token
      const argsGet: UserTokensModel.FindManyUserTokensArgs = {
        where: {
          AND: [{ token: { equals: data.refresh_token } }],
        },
      }

      const selectGet: any = {
        select: {
          user_id: true,
          token: true,
        },
      }
      const userTokenResult = await this.userTokensService.get(
        argsGet,
        selectGet,
      )

      if (userTokenResult instanceof Error)
        return new BadRequestException('Can not find refresh token')

      // Find user for gen new token
      const userResult: UsersModel.UsersWithoutPassword = await this.getOne(
        {
          where: {
            user_id: { equals: userTokenResult[0].user_id },
          },
        },
        {
          select: {
            user_id: true,
            name: true,
          },
        },
      )
      if (userResult === null) return new BadRequestException('User not found')

      const newToken = await this.authService.generate(userResult)

      return {
        access_token: newToken.access_token,
        refresh_token: userTokenResult[0].token,
      }
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async updateImage(
    args: UsersModel.UpdateOneUserImageArgs,
    select: any,
    user: UsersModel.Users,
  ): Promise<UsersModel.UsersWithoutPassword | Error> {
    /**
     * Update or create Cameras record
     */
    try {
      if (args.where)
        args.where = {
          ...args.where,
          user_id: user.user_id,
        }
      else
        args = {
          ...args,
          where: { user_id: user.user_id },
        }

      const argsUser: UsersModel.FindFirstUsersArgs = {
        where: { user_id: { equals: args.where.user_id } },
      }
      const selectUser: any = {
        select: {
          user_id: true,
          profile_pic: true,
        },
      }
      const resultUser = await this.getOne(argsUser, selectUser)
      if (resultUser instanceof Error) return resultUser

      if (resultUser.profile_pic !== '' && resultUser.profile_pic !== null) {
        const resultDelete = await this.minioClientService.delete(
          resultUser.profile_pic,
        )
        if (resultDelete instanceof Error) return resultDelete
      }

      // Convert base64 to buffer
      const fileName: any = await this.minioClientService.upload(
        args.data.profile_pic,
      )
      if (fileName instanceof Error) return fileName

      args.data.profile_pic = fileName

      return await this.prisma.users.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
