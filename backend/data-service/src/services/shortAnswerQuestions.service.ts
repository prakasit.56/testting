import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as ShortAnswerQuestionsModel from 'src/models/short-answer-questions'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class ShortAnswerQuestionsService {
  constructor(private prisma: PrismaService) {}

  async create(
    data: any,
  ): Promise<ShortAnswerQuestionsModel.ShortAnswerQuestions | Error> {
    /**
     * Create short answer question record
     */
    try {
      // Insert short answer question record to database
      return await this.prisma.shortAnswerQuestions.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: ShortAnswerQuestionsModel.FindManyShortAnswerQuestionsArgs,
    select: any,
  ): Promise<ShortAnswerQuestionsModel.ShortAnswerQuestions[] | Error> {
    /**
     * Get short answer question records
     */
    try {
      // Get short answer question records from database
      const result = await this.prisma.shortAnswerQuestions.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: ShortAnswerQuestionsModel.UpdateOneShortAnswerQuestionsArgs,
    select: any,
  ): Promise<ShortAnswerQuestionsModel.ShortAnswerQuestions | Error> {
    /**
     * Update or create short answer question record
     */
    try {
      // Update short answer question record
      return await this.prisma.shortAnswerQuestions.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: ShortAnswerQuestionsModel.DeleteOneShortAnswerQuestionsArgs,
  ): Promise<ShortAnswerQuestionsModel.ShortAnswerQuestions | Error> {
    /**
     * Delete short answer question record
     */
    try {
      // Delete short answer question record
      return await this.prisma.shortAnswerQuestions.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
