import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as MultipleQuestionsModel from 'src/models/multiple-questions'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class MultipleQuestionsService {
  constructor(private prisma: PrismaService) {}

  async create(
    data: any,
  ): Promise<MultipleQuestionsModel.MultipleQuestions | Error> {
    /**
     * Create multiple question record
     */
    try {
      // Insert multiple question record to database
      return await this.prisma.multipleQuestions.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: MultipleQuestionsModel.FindManyMultipleQuestionsArgs,
    select: any,
  ): Promise<MultipleQuestionsModel.MultipleQuestions[] | Error> {
    /**
     * Get multiple question records
     */
    try {
      // Get multiple question records from database
      const result = await this.prisma.multipleQuestions.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: MultipleQuestionsModel.UpdateOneMultipleQuestionsArgs,
    select: any,
  ): Promise<MultipleQuestionsModel.MultipleQuestions | Error> {
    /**
     * Update or create multiple question record
     */
    try {
      // Update multiple question record
      return await this.prisma.multipleQuestions.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: MultipleQuestionsModel.DeleteOneMultipleQuestionsArgs,
  ): Promise<MultipleQuestionsModel.MultipleQuestions | Error> {
    /**
     * Delete multiple question record
     */
    try {
      // Delete multiple question record
      return await this.prisma.multipleQuestions.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
