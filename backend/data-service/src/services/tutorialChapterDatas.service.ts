import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as TutorialChapterDatasModel from 'src/models/tutorial-chapter-datas'
import * as UsersModel from 'src/models/users'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class TutorialChapterDatasService {
  constructor(private prisma: PrismaService) {}

  async create(
    data: any,
  ): Promise<TutorialChapterDatasModel.TutorialChapterDatas | Error> {
    /**
     * Create tutorial chapter data record
     */
    try {
      // Insert tutorial chapter data record to database
      return await this.prisma.tutorialChapterDatas.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: TutorialChapterDatasModel.FindManyTutorialChapterDatasArgs,
    select: any,
  ): Promise<TutorialChapterDatasModel.TutorialChapterDatas[] | Error> {
    /**
     * Get tutorial chapter data records
     */
    try {
      // Get tutorial chapter data records from database
      const result = await this.prisma.tutorialChapterDatas.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: TutorialChapterDatasModel.UpdateOneTutorialChapterDatasArgs,
    select: any,
  ): Promise<TutorialChapterDatasModel.TutorialChapterDatas | Error> {
    /**
     * Update or create tutorial chapter data record
     */
    try {
      // Update tutorial chapter data record
      return await this.prisma.tutorialChapterDatas.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: TutorialChapterDatasModel.DeleteOneTutorialChapterDatasArgs,
  ): Promise<TutorialChapterDatasModel.TutorialChapterDatas | Error> {
    /**
     * Delete tutorial chapter data record
     */
    try {
      // Delete tutorial chapter data record
      return await this.prisma.tutorialChapterDatas.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async autoGrade(
    args: TutorialChapterDatasModel.TutorialAutoGrade,
    user: UsersModel.Users,
  ): Promise<boolean | Error> {
    /**
     * Auto grade tutorial in each chapter record
     */
    try {
      const result = await this.prisma.tutorialChapterDatas.findFirst({
        where: {
          chapter_id: args.chapterId,
        },
        select: {
          answer: true,
        },
      })

      if (result.answer === args.answer) {
        const chp_result = await this.prisma.userTutorials.create({
          data: {
            users: { connect: { user_id: user.user_id } },
            tutorialChapterDatas: { connect: { chapter_id: args.chapterId } },
          },
        })

        if (chp_result instanceof Error)
          return new InternalServerErrorException(chp_result.message)
      }

      return true
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
