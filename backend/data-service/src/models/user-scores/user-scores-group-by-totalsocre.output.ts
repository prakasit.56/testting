import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class UserScoresGroupByForTotalScore {
  @Field(() => String, { nullable: false })
  name!: string

  @Field(() => String, { nullable: true })
  totalScore!: string

  @Field(() => String, { nullable: true })
  timeScore!: string

  @Field(() => String, { nullable: false })
  numChallenge!: string
}
