import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersUpdateOneRequiredWithoutUserScoresInput } from '../users/users-update-one-required-without-user-scores.input'
import { ChallengesUpdateOneRequiredWithoutUserScoresInput } from '../challenges/challenges-update-one-required-without-user-scores.input'
import { IntFieldUpdateOperationsInput } from '../prisma/int-field-update-operations.input'
import { FloatFieldUpdateOperationsInput } from '../prisma/float-field-update-operations.input'
import { LevelsUpdateOneWithoutUserScoresInput } from '../levels/levels-update-one-without-user-scores.input'
import { CategoriesUpdateOneWithoutUserScoresInput } from '../categories/categories-update-one-without-user-scores.input'
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input'

@InputType()
export class UserScoresUpdateWithoutTypesInput {
  @Field(() => UsersUpdateOneRequiredWithoutUserScoresInput, { nullable: true })
  users?: UsersUpdateOneRequiredWithoutUserScoresInput

  @Field(() => ChallengesUpdateOneRequiredWithoutUserScoresInput, {
    nullable: true,
  })
  challenges?: ChallengesUpdateOneRequiredWithoutUserScoresInput

  @Field(() => IntFieldUpdateOperationsInput, { nullable: true })
  base_score?: IntFieldUpdateOperationsInput

  @Field(() => FloatFieldUpdateOperationsInput, { nullable: true })
  time_score?: FloatFieldUpdateOperationsInput

  @Field(() => LevelsUpdateOneWithoutUserScoresInput, { nullable: true })
  levels?: LevelsUpdateOneWithoutUserScoresInput

  @Field(() => CategoriesUpdateOneWithoutUserScoresInput, { nullable: true })
  categories?: CategoriesUpdateOneWithoutUserScoresInput

  @Field(() => DateTimeFieldUpdateOperationsInput, { nullable: true })
  createdAt?: DateTimeFieldUpdateOperationsInput

  @Field(() => DateTimeFieldUpdateOperationsInput, { nullable: true })
  updatedAt?: DateTimeFieldUpdateOperationsInput
}
