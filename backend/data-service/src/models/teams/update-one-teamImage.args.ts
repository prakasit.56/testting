import { Field, ArgsType } from '@nestjs/graphql'
import { TeamUpdateImageUserInput } from 'src/models/teams/team-update-image.input'
import { TeamsWhereUniqueInput } from 'src/models/teams/teams-where-unique.input'

@ArgsType()
export class UpdateOneTeamImageArgs {
  @Field(() => TeamUpdateImageUserInput, { nullable: false })
  data!: TeamUpdateImageUserInput

  @Field(() => TeamsWhereUniqueInput, { nullable: false })
  where!: TeamsWhereUniqueInput
}
