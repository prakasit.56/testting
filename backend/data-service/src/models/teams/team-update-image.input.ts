import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class TeamUpdateImageUserInput {
  @Field(() => String, { nullable: true })
  team_profile_pic!: string
}
