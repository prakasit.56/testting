import { Field, InputType, ID } from '@nestjs/graphql'

@InputType()
export class ShortAnswerQuestionAutoGrade {
  @Field(() => ID, { nullable: false })
  shortAnswerQuestionId!: string

  @Field(() => ID, { nullable: false })
  answer!: string

  @Field(() => Boolean, { nullable: false })
  hint!: boolean
}
