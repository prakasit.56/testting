import { Field, InputType, ID } from '@nestjs/graphql'

@InputType()
export class FlagQuestionAutoGrade {
  @Field(() => ID, { nullable: false })
  flagQuestionId!: string

  @Field(() => ID, { nullable: false })
  answer!: string

  @Field(() => Boolean, { nullable: false })
  hint!: boolean
}
