import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { StringFieldUpdateOperationsInput } from '../prisma/string-field-update-operations.input'
import { LevelsUpdateOneWithoutChallengesInput } from '../levels/levels-update-one-without-challenges.input'
import { TypesUpdateOneWithoutChallengesInput } from '../types/types-update-one-without-challenges.input'
import { CategoriesUpdateOneWithoutChallengesInput } from '../categories/categories-update-one-without-challenges.input'
import { BadgesUpdateOneWithoutChallengesInput } from '../badges/badges-update-one-without-challenges.input'
import { IntFieldUpdateOperationsInput } from '../prisma/int-field-update-operations.input'
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input'
import { UserScoresUpdateManyWithoutChallengesInput } from '../user-scores/user-scores-update-many-without-challenges.input'
import { ShortAnswerQuestionsUpdateManyWithoutChallengesInput } from '../short-answer-questions/short-answer-questions-update-many-without-challenges.input'
import { FlagQuestionsUpdateManyWithoutChallengesInput } from '../flag-questions/flag-questions-update-many-without-challenges.input'

@InputType()
export class ChallengesUpdateWithoutMultipleQuestionsInput {
  @Field(() => StringFieldUpdateOperationsInput, { nullable: true })
  challenge_id?: StringFieldUpdateOperationsInput

  @Field(() => StringFieldUpdateOperationsInput, { nullable: true })
  name?: StringFieldUpdateOperationsInput

  @Field(() => StringFieldUpdateOperationsInput, { nullable: true })
  question?: StringFieldUpdateOperationsInput

  @Field(() => StringFieldUpdateOperationsInput, { nullable: true })
  description?: StringFieldUpdateOperationsInput

  @Field(() => LevelsUpdateOneWithoutChallengesInput, { nullable: true })
  levels?: LevelsUpdateOneWithoutChallengesInput

  @Field(() => TypesUpdateOneWithoutChallengesInput, { nullable: true })
  types?: TypesUpdateOneWithoutChallengesInput

  @Field(() => CategoriesUpdateOneWithoutChallengesInput, { nullable: true })
  categories?: CategoriesUpdateOneWithoutChallengesInput

  @Field(() => BadgesUpdateOneWithoutChallengesInput, { nullable: true })
  badges?: BadgesUpdateOneWithoutChallengesInput

  @Field(() => IntFieldUpdateOperationsInput, { nullable: true })
  scorce?: IntFieldUpdateOperationsInput

  @Field(() => IntFieldUpdateOperationsInput, { nullable: true })
  max_time?: IntFieldUpdateOperationsInput

  @Field(() => IntFieldUpdateOperationsInput, { nullable: true })
  max_cost?: IntFieldUpdateOperationsInput

  @Field(() => DateTimeFieldUpdateOperationsInput, { nullable: true })
  createdAt?: DateTimeFieldUpdateOperationsInput

  @Field(() => DateTimeFieldUpdateOperationsInput, { nullable: true })
  updatedAt?: DateTimeFieldUpdateOperationsInput

  @Field(() => UserScoresUpdateManyWithoutChallengesInput, { nullable: true })
  userScores?: UserScoresUpdateManyWithoutChallengesInput

  @Field(() => ShortAnswerQuestionsUpdateManyWithoutChallengesInput, {
    nullable: true,
  })
  shortAnswerQuestions?: ShortAnswerQuestionsUpdateManyWithoutChallengesInput

  @Field(() => FlagQuestionsUpdateManyWithoutChallengesInput, {
    nullable: true,
  })
  flagQuestions?: FlagQuestionsUpdateManyWithoutChallengesInput
}
