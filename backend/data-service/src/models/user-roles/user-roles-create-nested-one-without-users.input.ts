import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserRolesCreateWithoutUsersInput } from './user-roles-create-without-users.input'
import { UserRolesCreateOrConnectWithoutUsersInput } from './user-roles-create-or-connect-without-users.input'
import { UserRolesWhereUniqueInput } from './user-roles-where-unique.input'

@InputType()
export class UserRolesCreateNestedOneWithoutUsersInput {
  @Field(() => UserRolesCreateWithoutUsersInput, { nullable: true })
  create?: UserRolesCreateWithoutUsersInput

  @Field(() => UserRolesCreateOrConnectWithoutUsersInput, { nullable: true })
  connectOrCreate?: UserRolesCreateOrConnectWithoutUsersInput

  @Field(() => UserRolesWhereUniqueInput, { nullable: true })
  connect?: UserRolesWhereUniqueInput
}
