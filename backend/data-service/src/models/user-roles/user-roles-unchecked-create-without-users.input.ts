import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'

@InputType()
export class UserRolesUncheckedCreateWithoutUsersInput {
  @Field(() => String, { nullable: true })
  user_role_id?: string

  @Field(() => String, { nullable: false })
  name!: string

  @Field(() => Date, { nullable: true })
  createdAt?: Date | string

  @Field(() => Date, { nullable: true })
  updatedAt?: Date | string
}
