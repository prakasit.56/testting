import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersUncheckedCreateNestedManyWithoutUser_roleInput } from '../users/users-unchecked-create-nested-many-without-user-role.input'

@InputType()
export class UserRolesUncheckedCreateInput {
  @Field(() => String, { nullable: true })
  user_role_id?: string

  @Field(() => String, { nullable: false })
  name!: string

  @Field(() => UsersUncheckedCreateNestedManyWithoutUser_roleInput, {
    nullable: true,
  })
  users?: UsersUncheckedCreateNestedManyWithoutUser_roleInput

  @Field(() => Date, { nullable: true })
  createdAt?: Date | string

  @Field(() => Date, { nullable: true })
  updatedAt?: Date | string
}
