import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserRolesCreateWithoutUsersInput } from './user-roles-create-without-users.input'
import { UserRolesCreateOrConnectWithoutUsersInput } from './user-roles-create-or-connect-without-users.input'
import { UserRolesUpsertWithoutUsersInput } from './user-roles-upsert-without-users.input'
import { UserRolesWhereUniqueInput } from './user-roles-where-unique.input'
import { UserRolesUpdateWithoutUsersInput } from './user-roles-update-without-users.input'

@InputType()
export class UserRolesUpdateOneRequiredWithoutUsersInput {
  @Field(() => UserRolesCreateWithoutUsersInput, { nullable: true })
  create?: UserRolesCreateWithoutUsersInput

  @Field(() => UserRolesCreateOrConnectWithoutUsersInput, { nullable: true })
  connectOrCreate?: UserRolesCreateOrConnectWithoutUsersInput

  @Field(() => UserRolesUpsertWithoutUsersInput, { nullable: true })
  upsert?: UserRolesUpsertWithoutUsersInput

  @Field(() => UserRolesWhereUniqueInput, { nullable: true })
  connect?: UserRolesWhereUniqueInput

  @Field(() => UserRolesUpdateWithoutUsersInput, { nullable: true })
  update?: UserRolesUpdateWithoutUsersInput
}
