import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserRolesCreateInput } from './user-roles-create.input'

@ArgsType()
export class CreateOneUserRolesArgs {
  @Field(() => UserRolesCreateInput, { nullable: false })
  data!: UserRolesCreateInput
}
