import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { StringFilter } from '../prisma/string-filter.input'
import { UsersListRelationFilter } from '../users/users-list-relation-filter.input'
import { DateTimeFilter } from '../prisma/date-time-filter.input'

@InputType()
export class UserRolesWhereInput {
  @Field(() => [UserRolesWhereInput], { nullable: true })
  AND?: Array<UserRolesWhereInput>

  @Field(() => [UserRolesWhereInput], { nullable: true })
  OR?: Array<UserRolesWhereInput>

  @Field(() => [UserRolesWhereInput], { nullable: true })
  NOT?: Array<UserRolesWhereInput>

  @Field(() => StringFilter, { nullable: true })
  user_role_id?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  name?: StringFilter

  @Field(() => UsersListRelationFilter, { nullable: true })
  users?: UsersListRelationFilter

  @Field(() => DateTimeFilter, { nullable: true })
  createdAt?: DateTimeFilter

  @Field(() => DateTimeFilter, { nullable: true })
  updatedAt?: DateTimeFilter
}
