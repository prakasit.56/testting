import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserRolesUpdateInput } from './user-roles-update.input'
import { UserRolesWhereUniqueInput } from './user-roles-where-unique.input'

@ArgsType()
export class UpdateOneUserRolesArgs {
  @Field(() => UserRolesUpdateInput, { nullable: false })
  data!: UserRolesUpdateInput

  @Field(() => UserRolesWhereUniqueInput, { nullable: false })
  where!: UserRolesWhereUniqueInput
}
