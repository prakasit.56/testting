import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'

@InputType()
export class UserRolesWhereUniqueInput {
  @Field(() => String, { nullable: true })
  user_role_id?: string
}
