import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserRolesUpdateWithoutUsersInput } from './user-roles-update-without-users.input'
import { UserRolesCreateWithoutUsersInput } from './user-roles-create-without-users.input'

@InputType()
export class UserRolesUpsertWithoutUsersInput {
  @Field(() => UserRolesUpdateWithoutUsersInput, { nullable: false })
  update!: UserRolesUpdateWithoutUsersInput

  @Field(() => UserRolesCreateWithoutUsersInput, { nullable: false })
  create!: UserRolesCreateWithoutUsersInput
}
