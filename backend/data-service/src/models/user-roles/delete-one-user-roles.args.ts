import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserRolesWhereUniqueInput } from './user-roles-where-unique.input'

@ArgsType()
export class DeleteOneUserRolesArgs {
  @Field(() => UserRolesWhereUniqueInput, { nullable: false })
  where!: UserRolesWhereUniqueInput
}
