import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserRolesWhereUniqueInput } from './user-roles-where-unique.input'
import { UserRolesCreateInput } from './user-roles-create.input'
import { UserRolesUpdateInput } from './user-roles-update.input'

@ArgsType()
export class UpsertOneUserRolesArgs {
  @Field(() => UserRolesWhereUniqueInput, { nullable: false })
  where!: UserRolesWhereUniqueInput

  @Field(() => UserRolesCreateInput, { nullable: false })
  create!: UserRolesCreateInput

  @Field(() => UserRolesUpdateInput, { nullable: false })
  update!: UserRolesUpdateInput
}
