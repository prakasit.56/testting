import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserRolesWhereInput } from './user-roles-where.input'

@ArgsType()
export class DeleteManyUserRolesArgs {
  @Field(() => UserRolesWhereInput, { nullable: true })
  where?: UserRolesWhereInput
}
