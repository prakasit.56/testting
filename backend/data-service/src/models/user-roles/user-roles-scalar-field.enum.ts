import { registerEnumType } from '@nestjs/graphql'

export enum UserRolesScalarFieldEnum {
  user_role_id = 'user_role_id',
  name = 'name',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}

registerEnumType(UserRolesScalarFieldEnum, {
  name: 'UserRolesScalarFieldEnum',
  description: undefined,
})
