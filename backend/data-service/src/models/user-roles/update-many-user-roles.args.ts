import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserRolesUpdateManyMutationInput } from './user-roles-update-many-mutation.input'
import { UserRolesWhereInput } from './user-roles-where.input'

@ArgsType()
export class UpdateManyUserRolesArgs {
  @Field(() => UserRolesUpdateManyMutationInput, { nullable: false })
  data!: UserRolesUpdateManyMutationInput

  @Field(() => UserRolesWhereInput, { nullable: true })
  where?: UserRolesWhereInput
}
