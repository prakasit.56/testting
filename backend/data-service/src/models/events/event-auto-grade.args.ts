import { Field, ArgsType, ID } from '@nestjs/graphql'

@ArgsType()
export class EventAutoGrade {
  @Field(() => ID, { nullable: false })
  eventId!: string

  @Field(() => String, { nullable: false })
  answer!: string
}
