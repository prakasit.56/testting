import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersCreateWithoutUser_roleInput } from './users-create-without-user-role.input'
import { UsersCreateOrConnectWithoutUser_roleInput } from './users-create-or-connect-without-user-role.input'
import { UsersUpsertWithWhereUniqueWithoutUser_roleInput } from './users-upsert-with-where-unique-without-user-role.input'
import { UsersCreateManyUser_roleInputEnvelope } from './users-create-many-user-role-input-envelope.input'
import { UsersWhereUniqueInput } from './users-where-unique.input'
import { UsersUpdateWithWhereUniqueWithoutUser_roleInput } from './users-update-with-where-unique-without-user-role.input'
import { UsersUpdateManyWithWhereWithoutUser_roleInput } from './users-update-many-with-where-without-user-role.input'
import { UsersScalarWhereInput } from './users-scalar-where.input'

@InputType()
export class UsersUpdateManyWithoutUser_roleInput {
  @Field(() => [UsersCreateWithoutUser_roleInput], { nullable: true })
  create?: Array<UsersCreateWithoutUser_roleInput>

  @Field(() => [UsersCreateOrConnectWithoutUser_roleInput], { nullable: true })
  connectOrCreate?: Array<UsersCreateOrConnectWithoutUser_roleInput>

  @Field(() => [UsersUpsertWithWhereUniqueWithoutUser_roleInput], {
    nullable: true,
  })
  upsert?: Array<UsersUpsertWithWhereUniqueWithoutUser_roleInput>

  @Field(() => UsersCreateManyUser_roleInputEnvelope, { nullable: true })
  createMany?: UsersCreateManyUser_roleInputEnvelope

  @Field(() => [UsersWhereUniqueInput], { nullable: true })
  set?: Array<UsersWhereUniqueInput>

  @Field(() => [UsersWhereUniqueInput], { nullable: true })
  disconnect?: Array<UsersWhereUniqueInput>

  @Field(() => [UsersWhereUniqueInput], { nullable: true })
  delete?: Array<UsersWhereUniqueInput>

  @Field(() => [UsersWhereUniqueInput], { nullable: true })
  connect?: Array<UsersWhereUniqueInput>

  @Field(() => [UsersUpdateWithWhereUniqueWithoutUser_roleInput], {
    nullable: true,
  })
  update?: Array<UsersUpdateWithWhereUniqueWithoutUser_roleInput>

  @Field(() => [UsersUpdateManyWithWhereWithoutUser_roleInput], {
    nullable: true,
  })
  updateMany?: Array<UsersUpdateManyWithWhereWithoutUser_roleInput>

  @Field(() => [UsersScalarWhereInput], { nullable: true })
  deleteMany?: Array<UsersScalarWhereInput>
}
