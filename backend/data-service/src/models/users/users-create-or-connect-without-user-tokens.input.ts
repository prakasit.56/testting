import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersWhereUniqueInput } from './users-where-unique.input'
import { UsersCreateWithoutUserTokensInput } from './users-create-without-user-tokens.input'

@InputType()
export class UsersCreateOrConnectWithoutUserTokensInput {
  @Field(() => UsersWhereUniqueInput, { nullable: false })
  where!: UsersWhereUniqueInput

  @Field(() => UsersCreateWithoutUserTokensInput, { nullable: false })
  create!: UsersCreateWithoutUserTokensInput
}
