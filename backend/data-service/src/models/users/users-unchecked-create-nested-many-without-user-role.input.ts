import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersCreateWithoutUser_roleInput } from './users-create-without-user-role.input'
import { UsersCreateOrConnectWithoutUser_roleInput } from './users-create-or-connect-without-user-role.input'
import { UsersCreateManyUser_roleInputEnvelope } from './users-create-many-user-role-input-envelope.input'
import { UsersWhereUniqueInput } from './users-where-unique.input'

@InputType()
export class UsersUncheckedCreateNestedManyWithoutUser_roleInput {
  @Field(() => [UsersCreateWithoutUser_roleInput], { nullable: true })
  create?: Array<UsersCreateWithoutUser_roleInput>

  @Field(() => [UsersCreateOrConnectWithoutUser_roleInput], { nullable: true })
  connectOrCreate?: Array<UsersCreateOrConnectWithoutUser_roleInput>

  @Field(() => UsersCreateManyUser_roleInputEnvelope, { nullable: true })
  createMany?: UsersCreateManyUser_roleInputEnvelope

  @Field(() => [UsersWhereUniqueInput], { nullable: true })
  connect?: Array<UsersWhereUniqueInput>
}
