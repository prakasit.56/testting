import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersCreateWithoutUserTokensInput } from './users-create-without-user-tokens.input'
import { UsersCreateOrConnectWithoutUserTokensInput } from './users-create-or-connect-without-user-tokens.input'
import { UsersUpsertWithoutUserTokensInput } from './users-upsert-without-user-tokens.input'
import { UsersWhereUniqueInput } from './users-where-unique.input'
import { UsersUpdateWithoutUserTokensInput } from './users-update-without-user-tokens.input'

@InputType()
export class UsersUpdateOneRequiredWithoutUserTokensInput {
  @Field(() => UsersCreateWithoutUserTokensInput, { nullable: true })
  create?: UsersCreateWithoutUserTokensInput

  @Field(() => UsersCreateOrConnectWithoutUserTokensInput, { nullable: true })
  connectOrCreate?: UsersCreateOrConnectWithoutUserTokensInput

  @Field(() => UsersUpsertWithoutUserTokensInput, { nullable: true })
  upsert?: UsersUpsertWithoutUserTokensInput

  @Field(() => UsersWhereUniqueInput, { nullable: true })
  connect?: UsersWhereUniqueInput

  @Field(() => UsersUpdateWithoutUserTokensInput, { nullable: true })
  update?: UsersUpdateWithoutUserTokensInput
}
