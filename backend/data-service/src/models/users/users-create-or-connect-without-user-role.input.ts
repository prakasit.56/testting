import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersWhereUniqueInput } from './users-where-unique.input'
import { UsersCreateWithoutUser_roleInput } from './users-create-without-user-role.input'

@InputType()
export class UsersCreateOrConnectWithoutUser_roleInput {
  @Field(() => UsersWhereUniqueInput, { nullable: false })
  where!: UsersWhereUniqueInput

  @Field(() => UsersCreateWithoutUser_roleInput, { nullable: false })
  create!: UsersCreateWithoutUser_roleInput
}
