import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersScalarWhereInput } from './users-scalar-where.input'
import { UsersUpdateManyMutationInput } from './users-update-many-mutation.input'

@InputType()
export class UsersUpdateManyWithWhereWithoutUser_roleInput {
  @Field(() => UsersScalarWhereInput, { nullable: false })
  where!: UsersScalarWhereInput

  @Field(() => UsersUpdateManyMutationInput, { nullable: false })
  data!: UsersUpdateManyMutationInput
}
