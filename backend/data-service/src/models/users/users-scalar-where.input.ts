import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { StringFilter } from '../prisma/string-filter.input'
import { IntFilter } from '../prisma/int-filter.input'
import { StringNullableFilter } from '../prisma/string-nullable-filter.input'
import { FloatFilter } from '../prisma/float-filter.input'
import { BoolFilter } from '../prisma/bool-filter.input'
import { DateTimeFilter } from '../prisma/date-time-filter.input'

@InputType()
export class UsersScalarWhereInput {
  @Field(() => [UsersScalarWhereInput], { nullable: true })
  AND?: Array<UsersScalarWhereInput>

  @Field(() => [UsersScalarWhereInput], { nullable: true })
  OR?: Array<UsersScalarWhereInput>

  @Field(() => [UsersScalarWhereInput], { nullable: true })
  NOT?: Array<UsersScalarWhereInput>

  @Field(() => StringFilter, { nullable: true })
  user_id?: StringFilter

  @Field(() => IntFilter, { nullable: true })
  user_invite_id?: IntFilter

  @Field(() => StringNullableFilter, { nullable: true })
  profile_pic?: StringNullableFilter

  @Field(() => FloatFilter, { nullable: true })
  total_score?: FloatFilter

  @Field(() => StringFilter, { nullable: true })
  type?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  name?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  username?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  password?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  email?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  conuntry_code?: StringFilter

  @Field(() => BoolFilter, { nullable: true })
  banned?: BoolFilter

  @Field(() => BoolFilter, { nullable: true })
  verified?: BoolFilter

  @Field(() => BoolFilter, { nullable: true })
  quick_start_status?: BoolFilter

  @Field(() => StringFilter, { nullable: true })
  profile_description?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  github_link?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  twitter_link?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  facebook_link?: StringFilter

  @Field(() => DateTimeFilter, { nullable: true })
  createdAt?: DateTimeFilter

  @Field(() => DateTimeFilter, { nullable: true })
  updatedAt?: DateTimeFilter

  @Field(() => StringFilter, { nullable: true })
  user_role_id?: StringFilter
}
