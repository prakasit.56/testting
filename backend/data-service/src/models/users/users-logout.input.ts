import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class UsersLogoutInput {
  @Field(() => String, { nullable: true })
  user_id: string

  @Field(() => String, { nullable: true })
  refresh_token: string
}
