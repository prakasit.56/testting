import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class UserUpdateImageUserInput {
  @Field(() => String, { nullable: true })
  profile_pic!: string
}
