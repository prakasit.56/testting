import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { SortOrder } from '../prisma/sort-order.enum'
import { UserTokensCountOrderByAggregateInput } from './user-tokens-count-order-by-aggregate.input'
import { UserTokensMaxOrderByAggregateInput } from './user-tokens-max-order-by-aggregate.input'
import { UserTokensMinOrderByAggregateInput } from './user-tokens-min-order-by-aggregate.input'

@InputType()
export class UserTokensOrderByWithAggregationInput {
  @Field(() => SortOrder, { nullable: true })
  token_id?: keyof typeof SortOrder

  @Field(() => SortOrder, { nullable: true })
  user_id?: keyof typeof SortOrder

  @Field(() => SortOrder, { nullable: true })
  token?: keyof typeof SortOrder

  @Field(() => SortOrder, { nullable: true })
  createdAt?: keyof typeof SortOrder

  @Field(() => SortOrder, { nullable: true })
  updatedAt?: keyof typeof SortOrder

  @Field(() => UserTokensCountOrderByAggregateInput, { nullable: true })
  _count?: UserTokensCountOrderByAggregateInput

  @Field(() => UserTokensMaxOrderByAggregateInput, { nullable: true })
  _max?: UserTokensMaxOrderByAggregateInput

  @Field(() => UserTokensMinOrderByAggregateInput, { nullable: true })
  _min?: UserTokensMinOrderByAggregateInput
}
