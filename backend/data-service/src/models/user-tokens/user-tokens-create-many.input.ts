import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'

@InputType()
export class UserTokensCreateManyInput {
  @Field(() => String, { nullable: true })
  token_id?: string

  @Field(() => String, { nullable: false })
  user_id!: string

  @Field(() => String, { nullable: false })
  token!: string

  @Field(() => Date, { nullable: true })
  createdAt?: Date | string

  @Field(() => Date, { nullable: true })
  updatedAt?: Date | string
}
