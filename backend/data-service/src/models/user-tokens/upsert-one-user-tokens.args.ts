import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserTokensWhereUniqueInput } from './user-tokens-where-unique.input'
import { UserTokensCreateInput } from './user-tokens-create.input'
import { UserTokensUpdateInput } from './user-tokens-update.input'

@ArgsType()
export class UpsertOneUserTokensArgs {
  @Field(() => UserTokensWhereUniqueInput, { nullable: false })
  where!: UserTokensWhereUniqueInput

  @Field(() => UserTokensCreateInput, { nullable: false })
  create!: UserTokensCreateInput

  @Field(() => UserTokensUpdateInput, { nullable: false })
  update!: UserTokensUpdateInput
}
