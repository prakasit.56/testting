import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { StringFilter } from '../prisma/string-filter.input'
import { DateTimeFilter } from '../prisma/date-time-filter.input'

@InputType()
export class UserTokensScalarWhereInput {
  @Field(() => [UserTokensScalarWhereInput], { nullable: true })
  AND?: Array<UserTokensScalarWhereInput>

  @Field(() => [UserTokensScalarWhereInput], { nullable: true })
  OR?: Array<UserTokensScalarWhereInput>

  @Field(() => [UserTokensScalarWhereInput], { nullable: true })
  NOT?: Array<UserTokensScalarWhereInput>

  @Field(() => StringFilter, { nullable: true })
  token_id?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  user_id?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  token?: StringFilter

  @Field(() => DateTimeFilter, { nullable: true })
  createdAt?: DateTimeFilter

  @Field(() => DateTimeFilter, { nullable: true })
  updatedAt?: DateTimeFilter
}
