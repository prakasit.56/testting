import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class UserRefreshTokensInput {
  @Field(() => String, { nullable: true })
  refresh_token?: string
}
