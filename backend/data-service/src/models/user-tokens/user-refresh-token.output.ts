import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class UserRefreshTokensOutput {
  @Field(() => String, { nullable: true })
  refresh_token?: string

  @Field(() => String, { nullable: true })
  access_token?: string
}
