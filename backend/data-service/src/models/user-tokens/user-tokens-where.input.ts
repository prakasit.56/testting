import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { StringFilter } from '../prisma/string-filter.input'
import { UsersRelationFilter } from '../users/users-relation-filter.input'
import { DateTimeFilter } from '../prisma/date-time-filter.input'

@InputType()
export class UserTokensWhereInput {
  @Field(() => [UserTokensWhereInput], { nullable: true })
  AND?: Array<UserTokensWhereInput>

  @Field(() => [UserTokensWhereInput], { nullable: true })
  OR?: Array<UserTokensWhereInput>

  @Field(() => [UserTokensWhereInput], { nullable: true })
  NOT?: Array<UserTokensWhereInput>

  @Field(() => StringFilter, { nullable: true })
  token_id?: StringFilter

  @Field(() => UsersRelationFilter, { nullable: true })
  users?: UsersRelationFilter

  @Field(() => StringFilter, { nullable: true })
  user_id?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  token?: StringFilter

  @Field(() => DateTimeFilter, { nullable: true })
  createdAt?: DateTimeFilter

  @Field(() => DateTimeFilter, { nullable: true })
  updatedAt?: DateTimeFilter
}
