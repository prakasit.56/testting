import { Field } from '@nestjs/graphql'
import { ObjectType } from '@nestjs/graphql'

@ObjectType()
export class UserTokensMinAggregate {
  @Field(() => String, { nullable: true })
  token_id?: string

  @Field(() => String, { nullable: true })
  user_id?: string

  @Field(() => String, { nullable: true })
  token?: string

  @Field(() => Date, { nullable: true })
  createdAt?: Date | string

  @Field(() => Date, { nullable: true })
  updatedAt?: Date | string
}
