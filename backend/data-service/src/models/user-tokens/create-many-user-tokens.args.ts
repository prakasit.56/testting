import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserTokensCreateManyInput } from './user-tokens-create-many.input'

@ArgsType()
export class CreateManyUserTokensArgs {
  @Field(() => [UserTokensCreateManyInput], { nullable: false })
  data!: Array<UserTokensCreateManyInput>

  @Field(() => Boolean, { nullable: true })
  skipDuplicates?: boolean
}
