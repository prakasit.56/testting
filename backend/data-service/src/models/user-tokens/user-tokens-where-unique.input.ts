import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'

@InputType()
export class UserTokensWhereUniqueInput {
  @Field(() => String, { nullable: true })
  token_id?: string
}
