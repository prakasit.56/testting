import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserTokensCreateManyUsersInput } from './user-tokens-create-many-users.input'

@InputType()
export class UserTokensCreateManyUsersInputEnvelope {
  @Field(() => [UserTokensCreateManyUsersInput], { nullable: false })
  data!: Array<UserTokensCreateManyUsersInput>

  @Field(() => Boolean, { nullable: true })
  skipDuplicates?: boolean
}
