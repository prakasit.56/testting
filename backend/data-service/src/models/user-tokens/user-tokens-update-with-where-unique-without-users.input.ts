import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserTokensWhereUniqueInput } from './user-tokens-where-unique.input'
import { UserTokensUpdateWithoutUsersInput } from './user-tokens-update-without-users.input'

@InputType()
export class UserTokensUpdateWithWhereUniqueWithoutUsersInput {
  @Field(() => UserTokensWhereUniqueInput, { nullable: false })
  where!: UserTokensWhereUniqueInput

  @Field(() => UserTokensUpdateWithoutUsersInput, { nullable: false })
  data!: UserTokensUpdateWithoutUsersInput
}
