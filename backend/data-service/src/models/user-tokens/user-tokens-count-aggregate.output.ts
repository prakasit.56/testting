import { Field } from '@nestjs/graphql'
import { ObjectType } from '@nestjs/graphql'
import { Int } from '@nestjs/graphql'

@ObjectType()
export class UserTokensCountAggregate {
  @Field(() => Int, { nullable: false })
  token_id!: number

  @Field(() => Int, { nullable: false })
  user_id!: number

  @Field(() => Int, { nullable: false })
  token!: number

  @Field(() => Int, { nullable: false })
  createdAt!: number

  @Field(() => Int, { nullable: false })
  updatedAt!: number

  @Field(() => Int, { nullable: false })
  _all!: number
}
