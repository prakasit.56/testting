import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserTokensWhereInput } from './user-tokens-where.input'
import { UserTokensOrderByWithRelationInput } from './user-tokens-order-by-with-relation.input'
import { UserTokensWhereUniqueInput } from './user-tokens-where-unique.input'
import { Int } from '@nestjs/graphql'
import { UserTokensCountAggregateInput } from './user-tokens-count-aggregate.input'
import { UserTokensMinAggregateInput } from './user-tokens-min-aggregate.input'
import { UserTokensMaxAggregateInput } from './user-tokens-max-aggregate.input'

@ArgsType()
export class UserTokensAggregateArgs {
  @Field(() => UserTokensWhereInput, { nullable: true })
  where?: UserTokensWhereInput

  @Field(() => [UserTokensOrderByWithRelationInput], { nullable: true })
  orderBy?: Array<UserTokensOrderByWithRelationInput>

  @Field(() => UserTokensWhereUniqueInput, { nullable: true })
  cursor?: UserTokensWhereUniqueInput

  @Field(() => Int, { nullable: true })
  take?: number

  @Field(() => Int, { nullable: true })
  skip?: number

  @Field(() => UserTokensCountAggregateInput, { nullable: true })
  _count?: UserTokensCountAggregateInput

  @Field(() => UserTokensMinAggregateInput, { nullable: true })
  _min?: UserTokensMinAggregateInput

  @Field(() => UserTokensMaxAggregateInput, { nullable: true })
  _max?: UserTokensMaxAggregateInput
}
