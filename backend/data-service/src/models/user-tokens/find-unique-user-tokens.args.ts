import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserTokensWhereUniqueInput } from './user-tokens-where-unique.input'

@ArgsType()
export class FindUniqueUserTokensArgs {
  @Field(() => UserTokensWhereUniqueInput, { nullable: false })
  where!: UserTokensWhereUniqueInput
}
