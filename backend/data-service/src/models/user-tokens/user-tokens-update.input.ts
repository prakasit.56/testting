import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { StringFieldUpdateOperationsInput } from '../prisma/string-field-update-operations.input'
import { UsersUpdateOneRequiredWithoutUserTokensInput } from '../users/users-update-one-required-without-user-tokens.input'
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input'

@InputType()
export class UserTokensUpdateInput {
  @Field(() => StringFieldUpdateOperationsInput, { nullable: true })
  token_id?: StringFieldUpdateOperationsInput

  @Field(() => UsersUpdateOneRequiredWithoutUserTokensInput, { nullable: true })
  users?: UsersUpdateOneRequiredWithoutUserTokensInput

  @Field(() => StringFieldUpdateOperationsInput, { nullable: true })
  token?: StringFieldUpdateOperationsInput

  @Field(() => DateTimeFieldUpdateOperationsInput, { nullable: true })
  createdAt?: DateTimeFieldUpdateOperationsInput

  @Field(() => DateTimeFieldUpdateOperationsInput, { nullable: true })
  updatedAt?: DateTimeFieldUpdateOperationsInput
}
