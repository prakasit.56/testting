import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserTokensCreateInput } from './user-tokens-create.input'

@ArgsType()
export class CreateOneUserTokensArgs {
  @Field(() => UserTokensCreateInput, { nullable: false })
  data!: UserTokensCreateInput
}
