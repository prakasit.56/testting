import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserTokensScalarWhereInput } from './user-tokens-scalar-where.input'
import { UserTokensUpdateManyMutationInput } from './user-tokens-update-many-mutation.input'

@InputType()
export class UserTokensUpdateManyWithWhereWithoutUsersInput {
  @Field(() => UserTokensScalarWhereInput, { nullable: false })
  where!: UserTokensScalarWhereInput

  @Field(() => UserTokensUpdateManyMutationInput, { nullable: false })
  data!: UserTokensUpdateManyMutationInput
}
