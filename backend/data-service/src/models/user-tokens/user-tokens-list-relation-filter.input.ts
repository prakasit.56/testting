import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserTokensWhereInput } from './user-tokens-where.input'

@InputType()
export class UserTokensListRelationFilter {
  @Field(() => UserTokensWhereInput, { nullable: true })
  every?: UserTokensWhereInput

  @Field(() => UserTokensWhereInput, { nullable: true })
  some?: UserTokensWhereInput

  @Field(() => UserTokensWhereInput, { nullable: true })
  none?: UserTokensWhereInput
}
