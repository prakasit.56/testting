import { Field } from '@nestjs/graphql'
import { ObjectType } from '@nestjs/graphql'
import { ID } from '@nestjs/graphql'
import { Users } from '../users/users.model'

@ObjectType()
export class UserTokens {
  @Field(() => ID, { nullable: false })
  token_id!: string

  @Field(() => Users, { nullable: false })
  users?: Users

  @Field(() => String, { nullable: false })
  user_id!: string

  @Field(() => String, { nullable: false })
  token!: string

  @Field(() => Date, { nullable: false })
  createdAt!: Date

  @Field(() => Date, { nullable: false })
  updatedAt!: Date
}
