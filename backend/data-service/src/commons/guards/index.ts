import * as auth from 'src/commons/guards/gql-auth.guard'
import * as role from 'src/commons/guards/gql-role.guard'

export { auth, role }
