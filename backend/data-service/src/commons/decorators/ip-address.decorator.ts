import { createParamDecorator, ExecutionContext } from '@nestjs/common'
import { GqlExecutionContext } from '@nestjs/graphql'

// import * as requestIp from 'request-ip'

export const IpAddress = createParamDecorator(
  (_, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context)
    const req = ctx.getContext().req
    if (req.clientIp) return req.clientIp as string
    return ''
  },
)
