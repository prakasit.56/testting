import { Injectable } from '@nestjs/common'
import { AuthService } from 'src/authentication/auth.service'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { ConfigService } from '@nestjs/config'
import * as UserModel from 'src/models/users'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT.SECRET_KEY'),
    })
  }

  async validate(data: {
    name: string
    sub: string
    iat: number
  }): Promise<UserModel.Users> {
    if (
      data.name === 'bypass' &&
      data.sub === this.configService.get<string>('JWT.BYPASS_SECRET_KEY')
    ) {
      return this.authService.verify(
        {
          name: 'jojo',
          sub: 'admin',
          iat: 123,
        },
        true,
      )
    }

    return this.authService.verify(data)
  }
}
