import { Injectable, UnauthorizedException } from '@nestjs/common'
import { AuthService } from 'src/authentication/auth.service'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { ConfigService } from '@nestjs/config'
import * as UserModel from 'src/models/users'

@Injectable()
export class JwtRefreshStrategy extends PassportStrategy(
  Strategy,
  'jwt-refresh',
) {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT.REFRESH_SECRET_KEY'),
    })
  }

  async validate(data: {
    name: string
    sub: string
    iat: number
  }): Promise<UserModel.Users> {
    // Verify user
    const user = await this.authService.verify(data)

    if (user instanceof Error) throw new UnauthorizedException()

    return user
  }
}
