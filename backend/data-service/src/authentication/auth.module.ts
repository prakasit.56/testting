import { Module, Logger } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { AuthService } from 'src/authentication/auth.service'
import { JwtModule } from '@nestjs/jwt'
import { configModuleConfig } from 'src/configs/configModule/configModule.config'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'
import { JwtStrategy } from 'src/authentication/strategies/jwt.strategy'

@Module({
  imports: [
    JwtModule.register({}),
    ConfigModule.forRoot(configModuleConfig),
    PrismaModule,
  ],
  providers: [Logger, AuthService, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
