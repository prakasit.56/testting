import { BadRequestException, Injectable, Logger } from '@nestjs/common'
import { MinioService } from 'nestjs-minio-client'
import * as crypto from 'crypto'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class MinioClientService {
  private readonly baseBucket = this.configService.get<string>('MINIO.BUCKET')

  private logger: Logger
  constructor(
    private readonly minio: MinioService,
    private readonly configService: ConfigService,
  ) {
    this.logger = new Logger('MinioClientService')
  }

  async upload(image: string): Promise<string | Error> {
    try {
      const buffer = Buffer.from(image, 'base64')
      const temp_filename = Date.now().toString()
      const hashedFileName = crypto
        .createHash('md5')
        .update(temp_filename)
        .digest('hex')
        .slice(16)
      const metaData = {
        'Content-Type': 'image/jpg',
        'X-Amz-Meta-Testing': 1234,
      }

      const fileName = `images/${hashedFileName}.jpg`

      await this.minio.client.putObject(
        this.baseBucket,
        fileName,
        buffer,
        metaData,
      )
      return fileName
    } catch (e) {
      return new BadRequestException(e.message)
    }
  }

  async delete(fileName: string): Promise<boolean | Error> {
    try {
      await this.minio.client.removeObject(this.baseBucket, fileName)
      return true
    } catch (e) {
      return new BadRequestException(e.message)
    }
  }
}
