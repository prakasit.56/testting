import { Injectable, InternalServerErrorException } from '@nestjs/common'
import * as ChallengeModel from 'src/models/challenges'
import { MailerService } from '@nestjs-modules/mailer'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class NodeMailerService {
  constructor(
    private readonly config: ConfigService,
    private readonly mailerService: MailerService,
  ) {}
  async sendEmailToRequestExam(
    exam: ChallengeModel.ChallengesCreateInput,
    receiverEmail: string,
  ): Promise<boolean | Error> {
    try {
      let text: string
      if (exam.flagQuestions) {
        text =
          `Challenges Name: ${exam.name}\n` +
          `Challenges Question: ${exam.question}\n` +
          `Challenges Desciption: ${exam.description}\n` +
          `Challenges Hit: ${exam.flagQuestions.create[0].hint}\n` +
          `Challenges Answer: ${exam.flagQuestions.create[0].answer}\n` +
          `Challenges Video Link: ${exam.flagQuestions.create[0].video_link}\n`
      }

      if (exam.shortAnswerQuestions) {
        text =
          `Challenges Name: ${exam.name}\n` +
          `Challenges Question: ${exam.question}\n` +
          `Challenges Desciption: ${exam.description}\n` +
          `Challenges Hit: ${exam.shortAnswerQuestions.create[0].hint}\n` +
          `Challenges Answer: ${exam.shortAnswerQuestions.create[0].answer}\n`
      }

      if (exam.multipleQuestions) {
        text =
          `Challenges Name: ${exam.name}\n` +
          `Challenges Question: ${exam.question}\n` +
          `Challenges Desciption: ${exam.description}\n` +
          `Challenges Hit: ${exam.multipleQuestions.create[0].hint}\n` +
          `Challenges Choice: ${exam.multipleQuestions.create[0].multipleChoiceQuestion.createMany.data[0].answer}, ${exam.multipleQuestions.create[0].multipleChoiceQuestion.createMany.data[0].correct_flag}\n` +
          `Challenges Choice: ${exam.multipleQuestions.create[0].multipleChoiceQuestion.createMany.data[1].answer}, ${exam.multipleQuestions.create[0].multipleChoiceQuestion.createMany.data[1].correct_flag}\n` +
          `Challenges Choice: ${exam.multipleQuestions.create[0].multipleChoiceQuestion.createMany.data[2].answer}, ${exam.multipleQuestions.create[0].multipleChoiceQuestion.createMany.data[2].correct_flag}\n` +
          `Challenges Choice: ${exam.multipleQuestions.create[0].multipleChoiceQuestion.createMany.data[3].answer}, ${exam.multipleQuestions.create[0].multipleChoiceQuestion.createMany.data[3].correct_flag}\n`
      }

      text = text + '\n\n' + `Best regards,\n` + `CPE32-CTF Playground team`
      await this.mailerService.sendMail({
        to: this.config.get<string>('MAIL.USER'),
        from: receiverEmail,
        subject: '[CPE-CTF Playground] Request for new exam',
        text: text,
      })
      return true
    } catch (e) {
      return new InternalServerErrorException(e.message)
    }
  }
}
