import { Module } from '@nestjs/common'
import { LevelsService } from 'src/services/levels.service'
import { LevelsResolver } from 'src/resolvers/levels.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [LevelsResolver, LevelsService],
})
export class LevelsModule {}
