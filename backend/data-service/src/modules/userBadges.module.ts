import { Module } from '@nestjs/common'
import { UserBadgesService } from 'src/services/userBadges.service'
import { UserBadgesResolver } from 'src/resolvers/userBadges.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [UserBadgesService, UserBadgesResolver],
})
export class UserBadgesModule {}
