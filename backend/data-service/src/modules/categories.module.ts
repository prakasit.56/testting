import { Module } from '@nestjs/common'
import { CategoriesService } from 'src/services/categories.service'
import { CategoriesResolver } from 'src/resolvers/categories.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [CategoriesResolver, CategoriesService],
})
export class CategoriesModule {}
