import { Module } from '@nestjs/common'
import { TypesService } from 'src/services/types.service'
import { TypesResolver } from 'src/resolvers/types.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [TypesResolver, TypesService],
})
export class TypesModule {}
