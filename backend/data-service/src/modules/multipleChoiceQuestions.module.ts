import { Module } from '@nestjs/common'
import { MultipleChoiceQuestionService } from 'src/services/multipleChoiceQuestion.service'
import { MultipleChoiceQuestionResolver } from 'src/resolvers/multipleChoiceQuestion.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [MultipleChoiceQuestionService, MultipleChoiceQuestionResolver],
})
export class MultipleChoiceQuestionsModule {}
