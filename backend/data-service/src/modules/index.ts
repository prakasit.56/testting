import { UsersModule } from 'src/modules/users.module'
import { UserLoggingsModule } from 'src/modules/userLoggings.module'
import { UserScoresModule } from 'src/modules/userScores.module'
import { UserTutorialsModule } from 'src/modules/userTutorial.module'
import { UserBadgesModule } from 'src/modules/userBadges.module'
import { TeamsModule } from 'src/modules/teams.module'
import { TeamMembersModule } from 'src/modules/teamMembers.module'
import { BadgesModule } from 'src/modules/badges.module'
import { TutorialContentsModule } from 'src/modules/tutorialContents.module'
import { TutorialChapterDatasModule } from 'src/modules/tutorialChapterDatas.module'
import { EventsModule } from 'src/modules/events.module'
import { LevelsModule } from 'src/modules/levels.module'
import { TypesModule } from 'src/modules/types.module'
import { CategoriesModule } from 'src/modules/categories.module'
import { ChallengesModule } from 'src/modules/challenges.module'
import { MultipleChoiceQuestionsModule } from 'src/modules/multipleChoiceQuestions.module'
import { MultipleQuestionsModule } from 'src/modules/multipleQuestions.module'
import { ShortAnswerQuestionsModule } from 'src/modules/shortAnswerQuestions.module'
import { FlagQuestionsModule } from 'src/modules/flagQuestions.module'

const modules = [
  UsersModule,
  UserLoggingsModule,
  UserScoresModule,
  UserBadgesModule,
  UserTutorialsModule,
  BadgesModule,
  TeamsModule,
  TeamMembersModule,
  TutorialContentsModule,
  TutorialChapterDatasModule,
  EventsModule,
  LevelsModule,
  TypesModule,
  CategoriesModule,
  ChallengesModule,
  MultipleChoiceQuestionsModule,
  MultipleQuestionsModule,
  ShortAnswerQuestionsModule,
  FlagQuestionsModule,
]

export default {
  modules,
}
