import { Module } from '@nestjs/common'
import { FlagQuestionsService } from 'src/services/flagQuestions.service'
import { FlagQuestionsResolver } from 'src/resolvers/flagQuestions.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [FlagQuestionsService, FlagQuestionsResolver],
})
export class FlagQuestionsModule {}
