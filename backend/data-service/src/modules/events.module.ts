import { Module } from '@nestjs/common'
import { EventsService } from 'src/services/events.service'
import { EventsResolver } from 'src/resolvers/events.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [EventsResolver, EventsService],
})
export class EventsModule {}
