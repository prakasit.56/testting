import { Module } from '@nestjs/common'
import { TutorialContentsService } from 'src/services/tutorialContents.service'
import { TutorialContentsResolver } from 'src/resolvers/tutorialContents.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [TutorialContentsService, TutorialContentsResolver],
})
export class TutorialContentsModule {}
