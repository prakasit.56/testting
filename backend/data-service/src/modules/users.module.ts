import { Module } from '@nestjs/common'
import { UsersService } from 'src/services/users.service'
import { UserTokensService } from 'src/services/userToken.service'
import { UsersResolver } from 'src/resolvers/users.resolvers'
import { AuthModule } from 'src/authentication/auth.module'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'
import { MinioClientModule } from 'src/providers/storage/minio/minio.module'

@Module({
  imports: [AuthModule, PrismaModule, MinioClientModule],
  providers: [UsersService, UserTokensService, UsersResolver],
})
export class UsersModule {}
