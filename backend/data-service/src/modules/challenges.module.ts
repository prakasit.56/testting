import { Module } from '@nestjs/common'
import { NodeMailerModule } from 'src/providers/node-mailer/node-mailer.module'
import { ChallengesService } from 'src/services/challenges.service'
import { ChallengesResolver } from 'src/resolvers/challenges.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule, NodeMailerModule],
  providers: [ChallengesResolver, ChallengesService],
})
export class ChallengesModule {}
