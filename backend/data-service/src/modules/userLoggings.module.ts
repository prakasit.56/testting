import { Module } from '@nestjs/common'
import { UserLoggingsService } from 'src/services/userLoggings.service'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [UserLoggingsService],
})
export class UserLoggingsModule {}
