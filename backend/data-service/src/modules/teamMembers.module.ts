import { Module } from '@nestjs/common'
import { TeamMembersService } from 'src/services/teamMembers.service'
import { TeamMembersResolver } from 'src/resolvers/teamMembers.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [TeamMembersService, TeamMembersResolver],
})
export class TeamMembersModule {}
