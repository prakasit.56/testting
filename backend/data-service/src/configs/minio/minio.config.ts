export const AWS_ENDPOINT = process.env.AWS_ENDPOINT
export const AWS_ACCESS_KEY = process.env.AWS_ACCESS_KEY
export const AWS_SECRET_KEY = process.env.AWS_SECRET_KEY
export const useSSL = true
export const BUCKET = process.env.BUCKET_NAME
export const REGION = 'ap-southeast-1'
