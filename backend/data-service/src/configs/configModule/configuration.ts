import { graphqlConfig } from 'src/configs/graphql/graphql.config'
import {
  AWS_ENDPOINT,
  AWS_ACCESS_KEY,
  AWS_SECRET_KEY,
  useSSL,
  BUCKET,
  REGION,
} from 'src/configs/minio/minio.config'
import {
  BYPASS_SECRET_KEY,
  SECRET_KEY,
  REFRESH_SECRET_KEY,
} from 'src/configs/jwt/jwt.config'
import { MAIL_CONFIG } from 'src/configs/mail/mail.config'

export default () => ({
  graphql: graphqlConfig,
  JWT: {
    SECRET_KEY: SECRET_KEY,
    REFRESH_SECRET_KEY: REFRESH_SECRET_KEY,
    BYPASS_SECRET_KEY: BYPASS_SECRET_KEY,
  },
  MINIO: {
    ENDPOINT: AWS_ENDPOINT,
    ACCESS_KEY: AWS_ACCESS_KEY,
    SECRET_KEY: AWS_SECRET_KEY,
    BUCKET: BUCKET,
    useSSL: useSSL,
    REGION: REGION,
  },
  MAIL: MAIL_CONFIG,
})
