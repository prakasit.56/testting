import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as LevelsModel from 'src/models/levels'
import { LevelsService } from 'src/services/levels.service'

@Resolver(() => LevelsModel.Levels)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class LevelsResolver {
  private logger: Logger
  constructor(private levelsService: LevelsService) {
    this.logger = new Logger('Levels Service')
  }

  /**
   * Get level records
   */
  @Query(() => [LevelsModel.Levels], {
    name: 'getLevels',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getLevels(
    @Args('') args: LevelsModel.FindManyLevelsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<LevelsModel.Levels[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.levelsService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create level record
   */
  @Mutation(() => LevelsModel.Levels, {
    name: 'createLevels',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createLevels(
    @Args('') args: LevelsModel.CreateOneLevelsArgs,
  ): Promise<LevelsModel.Levels | Error> {
    const result = await this.levelsService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update level record
   */
  @Mutation(() => LevelsModel.Levels, {
    name: 'updateLevels',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateLevels(
    @Args('') args: LevelsModel.UpdateOneLevelsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<LevelsModel.Levels | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.levelsService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete level record
   */
  @Mutation(() => LevelsModel.Levels, {
    name: 'deleteLevels',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteLevels(
    @Args('') args: LevelsModel.DeleteOneLevelsArgs,
  ): Promise<LevelsModel.Levels | Error> {
    const result = await this.levelsService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
