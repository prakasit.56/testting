import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as FlagQuestionsModel from 'src/models/flag-questions'
import { FlagQuestionsService } from 'src/services/flagQuestions.service'

@Resolver(() => FlagQuestionsModel.FlagQuestions)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class FlagQuestionsResolver {
  private logger: Logger
  constructor(private shortAnswerQuestionsService: FlagQuestionsService) {
    this.logger = new Logger('FlagQuestions Service')
  }

  /**
   * Get flagQuestions records
   */
  @Query(() => [FlagQuestionsModel.FlagQuestions], {
    name: 'getFlagQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getFlagQuestions(
    @Args('') args: FlagQuestionsModel.FindManyFlagQuestionsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<FlagQuestionsModel.FlagQuestions[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.shortAnswerQuestionsService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create flagQuestions record
   */
  @Mutation(() => FlagQuestionsModel.FlagQuestions, {
    name: 'createFlagQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createFlagQuestions(
    @Args('') args: FlagQuestionsModel.CreateOneFlagQuestionsArgs,
  ): Promise<FlagQuestionsModel.FlagQuestions | Error> {
    const result = await this.shortAnswerQuestionsService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update flagQuestions record
   */
  @Mutation(() => FlagQuestionsModel.FlagQuestions, {
    name: 'updateFlagQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateFlagQuestions(
    @Args('') args: FlagQuestionsModel.UpdateOneFlagQuestionsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<FlagQuestionsModel.FlagQuestions | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.shortAnswerQuestionsService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete flagQuestions record
   */
  @Mutation(() => FlagQuestionsModel.FlagQuestions, {
    name: 'deleteFlagQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteFlagQuestions(
    @Args('') args: FlagQuestionsModel.DeleteOneFlagQuestionsArgs,
  ): Promise<FlagQuestionsModel.FlagQuestions | Error> {
    const result = await this.shortAnswerQuestionsService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
