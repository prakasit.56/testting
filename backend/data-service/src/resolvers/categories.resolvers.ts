import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as CategoriesModel from 'src/models/categories'
import { CategoriesService } from 'src/services/categories.service'

@Resolver(() => CategoriesModel.Categories)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class CategoriesResolver {
  private logger: Logger
  constructor(private categoriesService: CategoriesService) {
    this.logger = new Logger('Categories Service')
  }

  /**
   * Get categories records
   */
  @Query(() => [CategoriesModel.Categories], {
    name: 'getCategories',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getCategories(
    @Args('') args: CategoriesModel.FindManyCategoriesArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<CategoriesModel.Categories[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.categoriesService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create categories record
   */
  @Mutation(() => CategoriesModel.Categories, {
    name: 'createCategories',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createCategories(
    @Args('') args: CategoriesModel.CreateOneCategoriesArgs,
  ): Promise<CategoriesModel.Categories | Error> {
    const result = await this.categoriesService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update categories record
   */
  @Mutation(() => CategoriesModel.Categories, {
    name: 'updateCategories',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateCategories(
    @Args('') args: CategoriesModel.UpdateOneCategoriesArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<CategoriesModel.Categories | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.categoriesService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete categories record
   */
  @Mutation(() => CategoriesModel.Categories, {
    name: 'deleteCategories',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteCategories(
    @Args('') args: CategoriesModel.DeleteOneCategoriesArgs,
  ): Promise<CategoriesModel.Categories | Error> {
    const result = await this.categoriesService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
