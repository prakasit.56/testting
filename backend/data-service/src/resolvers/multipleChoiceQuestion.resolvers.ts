import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as MultipleChoiceQuestionModel from 'src/models/multiple-choice-question'
import { MultipleChoiceQuestionService } from 'src/services/multipleChoiceQuestion.service'

@Resolver(() => MultipleChoiceQuestionModel.MultipleChoiceQuestion)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class MultipleChoiceQuestionResolver {
  private logger: Logger
  constructor(
    private multipleChoiceQuestionService: MultipleChoiceQuestionService,
  ) {
    this.logger = new Logger('MultipleChoiceQuestion Service')
  }

  /**
   * Get multiple choice question records
   */
  @Query(() => [MultipleChoiceQuestionModel.MultipleChoiceQuestion], {
    name: 'getMultipleChoiceQuestion',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getMultipleChoiceQuestion(
    @Args('')
    args: MultipleChoiceQuestionModel.FindManyMultipleChoiceQuestionArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<MultipleChoiceQuestionModel.MultipleChoiceQuestion[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.multipleChoiceQuestionService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create multiple choice question record
   */
  @Mutation(() => MultipleChoiceQuestionModel.MultipleChoiceQuestion, {
    name: 'createMultipleChoiceQuestion',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createMultipleChoiceQuestion(
    @Args('')
    args: MultipleChoiceQuestionModel.CreateOneMultipleChoiceQuestionArgs,
  ): Promise<MultipleChoiceQuestionModel.MultipleChoiceQuestion | Error> {
    const result = await this.multipleChoiceQuestionService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update multiple choice question record
   */
  @Mutation(() => MultipleChoiceQuestionModel.MultipleChoiceQuestion, {
    name: 'updateMultipleChoiceQuestion',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateMultipleChoiceQuestion(
    @Args('')
    args: MultipleChoiceQuestionModel.UpdateOneMultipleChoiceQuestionArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<MultipleChoiceQuestionModel.MultipleChoiceQuestion | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.multipleChoiceQuestionService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete multiple choice question record
   */
  @Mutation(() => MultipleChoiceQuestionModel.MultipleChoiceQuestion, {
    name: 'deleteMultipleChoiceQuestion',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteMultipleChoiceQuestion(
    @Args('')
    args: MultipleChoiceQuestionModel.DeleteOneMultipleChoiceQuestionArgs,
  ): Promise<MultipleChoiceQuestionModel.MultipleChoiceQuestion | Error> {
    const result = await this.multipleChoiceQuestionService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
