import { FC, PropsWithChildren } from "react";

//* Interfaces or Types
interface IComponentWithChildProps {
  children: PropsWithChildren<{ child?: string }>
}

const LayoutContent: FC<IComponentWithChildProps> = ({ children }) => {
  return (
    <>
      <div
        className="min-h-screen bg-secondary"
        style={{ minHeight: `calc(100vh)` }}
      >
        {children}
      </div>
    </>
  );
};

export default LayoutContent;
