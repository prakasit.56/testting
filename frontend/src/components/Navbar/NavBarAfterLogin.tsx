import React from "react";
import { useMutation } from 'urql'

import logo from "assets/ctf-logo.svg";
import { useQuery } from 'urql'
import { userAPI } from 'services/graphql';
import { teamAPI } from 'services/graphql';
import { general } from 'utils';

import OutsideClickHandler from 'react-outside-click-handler';


const NavBarAfterLogin = () => {

  const [user] = useQuery(userAPI.query.getUser())
  const [team] = useQuery(teamAPI.query.getTeam())

  const [SignOutResult, SignOut] = useMutation(userAPI.mutation.singOutUserMutation)

  const onClickLogout = () => {
    userAPI.handler.handleOnSubmitSignOut(SignOut, general.auth.getRefreshToken())
    console.log(SignOutResult)
  }
  
  const [LeaderboardDropdown, setShowLeaderboardDropdown] = React.useState(false);
  const [ChallengeDropdown, setShowChallengeDropdown] = React.useState(false);
  const [TeamDropdown, setShowTeamDropdown] = React.useState(false);
  const [ProfileDropdown, setShowProfileDropdown] = React.useState(false);

	const toggleLeaderBoardDropdown = () => {
		setShowLeaderboardDropdown(true);
    setShowChallengeDropdown(false);
    setShowTeamDropdown(false);
    setShowProfileDropdown(false)
	}

	const closeLeaderBoardDropdown = () => {
		setShowLeaderboardDropdown(false)
	}

  const toggleChallengeDropdown = () => {
		setShowLeaderboardDropdown(false);
    setShowChallengeDropdown(true);
    setShowTeamDropdown(false);
    setShowProfileDropdown(false)
	}

	const closeChallengeDropdown = () => {
		setShowChallengeDropdown(false)
	}

  const toggleTeamDropdown = () => {
		setShowLeaderboardDropdown(false);
    setShowChallengeDropdown(false);
    setShowTeamDropdown(true);
    setShowProfileDropdown(false)
	}

	const closeTeamDropdown = () => {
		setShowTeamDropdown(false)
	}

  const toggleProfileDropdown = () => {
		setShowLeaderboardDropdown(false);
    setShowChallengeDropdown(false);
    setShowTeamDropdown(false);
    setShowProfileDropdown(true)
	}

	const closeProfileDropdown = () => {
		setShowProfileDropdown(false)
	}

  return (
    
    <div className="flex h-20 items-center justify-between bg-[#0A0527] px-8 shadow-md">
      <div className="flex items-center justify-center">
      <button
            onClick={() =>  window.location.href='/main-event'}
          >
          <img src={logo} alt="ctfd-32" className="h-full w-20" />
        </button>

        <button
            onClick={() =>  window.location.href='/main-event'}
          >
          <h6 className="ml-24 text-[#0FB1D9] duration-300 ease-in-out hover:text-white font-Roboto text-base">
            Home
          </h6>
        </button>
      </div>
      <div className="flex gap-6">

        {/* <a href="/tutorial">
          <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white font-Roboto text-base">
            Tutorial
          </h6>
        </a> */}
        <div className="sticky">
          <button
            onClick={() =>  window.location.href='/tutorial' + `?user_id=${user.data.getUsers[0].user_id}`}
          >
            <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white inline-flex font-Roboto text-base">
              Tutorial
            </h6>
          </button>
        </div>
        {/* <a href="/signin">
          <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white inline-flex">
            Leaderboard
            <svg className="ml-2 w-5 h-5" fill="none" stroke="white" viewBox="0 0 24 18" xmlns="http://www.w3.org/2000/svg"><path strokeWidth="2" d="M19 9l-7 7-7-7"></path></svg>
          </h6>
        </a> */}
        <div className="sticky">
          
          <button
            // onClick={() => setShowLeaderboardDropdown(true)}
            onClick={() => toggleLeaderBoardDropdown()}
          >
            <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white inline-flex font-Roboto text-base">
              Leaderboard
              <svg className="ml-2 w-5 h-5" fill="none" stroke="white" viewBox="0 0 24 18" xmlns="http://www.w3.org/2000/svg"><path strokeWidth="2" d="M19 9l-7 7-7-7"></path></svg>
            </h6>
          </button>
          <button
            // className={
            //   LeaderboardDropdown ? (
            //     // 'cursor-default bg-black opacity-50 fixed inset-0 w-full h-full'
            //       'cursor-default fixed'
            //   ) : (
            //     'hidden'
            //   )
            // }
            className='cursor-default fixed'
            onClick={() => closeLeaderBoardDropdown()}
            tabIndex={-1}
          />
          {LeaderboardDropdown ? (
              
          <>
                  
          <div
            className={
              LeaderboardDropdown ? (
                // 'absolute right-0 mt-2 w-48 bg-white rounded-lg py-2 shadow-xl'
                  'absolute left-1/2 transform -translate-x-1/2 mt-2 w-48 bg-white rounded-lg py-2 shadow-xl z-50'
              ) : (
                'hidden'
              )
            }
            
          >
            <OutsideClickHandler
              onOutsideClick={() => {
                setShowLeaderboardDropdown(false);
              }}
            >
              <a href="/score-leaderboard" className="block px-4 py-2 text-gray-800 hover:bg-[#0062B9] hover:text-white transition-all duration-150 z-auto">
                Leaderboard
              </a>
              <a href="/team-score-leaderboard" className="block px-4 py-2 text-gray-800 hover:bg-[#0062B9] hover:text-white transition-all duration-150 z-auto">
                Team Leaderboard
              </a>
              <a href="/time-score-leaderboard" className="block px-4 py-2 text-gray-800 hover:bg-[#0062B9] hover:text-white transition-all duration-150 z-50">
                Time Score Leaderboard
              </a>
            </OutsideClickHandler>
          </div>
          
          </>
          
          ) : null}
        </div>
        <div className="sticky">
          <button
            onClick={() => toggleChallengeDropdown()}
          >
            <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white inline-flex font-Roboto text-base">
              Challenge
              <svg className="ml-2 w-5 h-5" fill="none" stroke="white" viewBox="0 0 24 18" xmlns="http://www.w3.org/2000/svg"><path strokeWidth="2" d="M19 9l-7 7-7-7"></path></svg>
            </h6>
          </button>
          <button
            // className={
            //   ChallengeDropdown ? (
            //     // 'cursor-default bg-black opacity-50 fixed inset-0 w-full h-full'
            //       'cursor-default fixed'
            //   ) : (
            //     'hidden'
            //   )
            // }
            className='cursor-default fixed'
            onClick={() => closeChallengeDropdown()}
            tabIndex={-1}
          />
          {ChallengeDropdown ? (
          <>
          <div
            className={
              ChallengeDropdown ? (
                // 'absolute right-0 mt-2 w-48 bg-white rounded-lg py-2 shadow-xl'
                  'absolute left-1/2 transform -translate-x-1/2 mt-2 w-48 bg-white rounded-lg py-2 shadow-xl'
              ) : (
                'hidden'
              )
            }
          >
            <OutsideClickHandler
              onOutsideClick={() => {
                setShowChallengeDropdown(false);
              }}
            >
              <a href="/main-challenge" className="block px-4 py-2 text-gray-800 hover:bg-[#0062B9] hover:text-white transition-all duration-150">
                Challenge
              </a>
              <a href="/create-challenge" className="block px-4 py-2 text-gray-800 hover:bg-[#0062B9] hover:text-white transition-all duration-150">
                Create Challenge
              </a>
            </OutsideClickHandler>
          </div>
          </>
          ) : null}
        </div>
        {/* <a href="/signin">
          <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white inline-flex">
            Team
            <svg className="ml-2 w-5 h-5" fill="none" stroke="white" viewBox="0 0 24 18" xmlns="http://www.w3.org/2000/svg"><path strokeWidth="2" d="M19 9l-7 7-7-7"></path></svg>
          </h6>
        </a> */}
        <div className="sticky">
          <button
            onClick={() => toggleTeamDropdown()}
          >
            <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white inline-flex font-Roboto text-base">
              Team
              <svg className="ml-2 w-5 h-5" fill="none" stroke="white" viewBox="0 0 24 18" xmlns="http://www.w3.org/2000/svg"><path strokeWidth="2" d="M19 9l-7 7-7-7"></path></svg>
            </h6>
          </button>
          <button
            // className={
            //   TeamDropdown ? (
            //     // 'cursor-default bg-black opacity-50 fixed inset-0 w-full h-full'
            //       'cursor-default fixed'
            //   ) : (
            //     'hidden'
            //   )
            // }
            className='cursor-default fixed'
            onClick={() => closeTeamDropdown()}
            tabIndex={-1}
          />
          {TeamDropdown ? (
          <>
          <div
            className={
              TeamDropdown ? (
                // 'absolute right-0 mt-2 w-48 bg-white rounded-lg py-2 shadow-xl'
                  'absolute left-1/2 transform -translate-x-1/2 mt-2 w-48 bg-white rounded-lg py-2 shadow-xl z-50'
              ) : (
                'hidden'
              )
            }
          >
            <OutsideClickHandler
              onOutsideClick={() => {
                setShowTeamDropdown(false);
              }}
            >
              <a href={"/team-profile" + `?team_id=${team.data.getTeams[0].team_id}`} className="block px-4 py-2 text-gray-800 hover:bg-[#0062B9] hover:text-white transition-all duration-150">
                My Team
              </a>
              <a href="/create-team" className="block px-4 py-2 text-gray-800 hover:bg-[#0062B9] hover:text-white transition-all duration-150">
                Create Team
              </a>
            </OutsideClickHandler>
          </div>
          </>
          ) : null}
        </div>
        {/* <a href="/signin">
          <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white inline-flex">
            Profile
            <svg className="ml-2 w-5 h-5" fill="none" stroke="white" viewBox="0 0 24 18" xmlns="http://www.w3.org/2000/svg"><path strokeWidth="2" d="M19 9l-7 7-7-7"></path></svg>
          </h6>
        </a> */}
        <div className="sticky">
          <button
            onClick={() => toggleProfileDropdown()}
          >
            <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white inline-flex font-Roboto text-base">
              Profile
              <svg className="ml-2 w-5 h-5" fill="none" stroke="white" viewBox="0 0 24 18" xmlns="http://www.w3.org/2000/svg"><path strokeWidth="2" d="M19 9l-7 7-7-7"></path></svg>
            </h6>
          </button>
          {ProfileDropdown ? (
          <>
          <button
            // className={
            //   ProfileDropdown ? (
            //     // 'cursor-default bg-black opacity-50 fixed inset-0 w-full h-full'
            //       'cursor-default fixed'
            //   ) : (
            //     'hidden'
            //   )
            // }
            className='cursor-default fixed'
            onClick={() => closeProfileDropdown()}
            tabIndex={-1}
          />
          <div
            className={
              ProfileDropdown ? (
                  'absolute right-0 mt-2 w-48 bg-white rounded-lg py-2 shadow-xl z-50'
                  //'absolute left-1/2 transform -translate-x-1/2 mt-2 w-48 bg-white rounded-lg py-2 shadow-xl'
              ) : (
                'hidden'
              )
            }
          >
            <OutsideClickHandler
              onOutsideClick={() => {
                setShowProfileDropdown(false);
              }}
            >
              <a href={"/profile" + `?profile_id=${user.data.getUsers[0].user_id}`} className="block px-4 py-2 text-gray-800 hover:bg-[#0062B9] hover:text-white transition-all duration-150">
                Profile
              </a>
              <a onClick={onClickLogout} href="#" className="block px-4 py-2 text-gray-800 hover:bg-[#0062B9] hover:text-white transition-all duration-150">
                Logout
              </a>
            </OutsideClickHandler>
          </div>
          </>
          ) : null}
        </div>
      </div>
    </div>
  );
}

export default NavBarAfterLogin;
