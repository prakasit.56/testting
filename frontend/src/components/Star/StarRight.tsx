import React from "react";
import { GrStar } from "react-icons/gr";

type StarRightProps = {
    className?: string;
};

function StarRight({ className }: StarRightProps) {
return (
    <div>
    <div className={`absolute top-1.5 right-0 flex flex-col ${className}`}>
        <div className="flex flex-col gap-3">
            <GrStar className="h-5 w-5 rotate-60" style={{color: '#FFD233', fontSize: '150px'}}/>
            <GrStar className="h-5 w-5 rotate-60" style={{color: '#FFD233', fontSize: '150px'}}/>
            <GrStar className="h-5 w-5 rotate-60" style={{color: '#FFD233', fontSize: '150px'}}/>
            <GrStar className="h-5 w-5 rotate-60" style={{color: '#FFD233', fontSize: '150px'}}/>
            <GrStar className="h-5 w-5 rotate-60" style={{color: '#FFD233', fontSize: '150px'}}/>
        </div>
    </div>
    </div>
);
}

export default StarRight;
