import React from "react";
import { GrStar } from "react-icons/gr";

type StarLeftProps = {
  className?: string;
};

function StarLeft({ className }: StarLeftProps) {
  return (
    <div>
      <div className={`bottom-15 absolute left-0 flex flex-col ${className}`}>
        <div className="flex flex-col gap-3">
          <GrStar className="h-5 w-5 rotate-30" style={{color: '#FFD233', fontSize: '150px'}}/>
          <GrStar className="h-5 w-5 rotate-60" style={{color: '#FFD233', fontSize: '150px'}}/>
          <GrStar className="h-5 w-5 rotate-60" style={{color: '#FFD233', fontSize: '150px'}}/>
          <GrStar className="h-5 w-5 rotate-60" style={{color: '#FFD233', fontSize: '150px'}}/>
          <GrStar className="h-5 w-5 rotate-60" style={{color: '#FFD233', fontSize: '150px'}}/>
        </div>
      </div>
    </div>
  );
}

export default StarLeft;
