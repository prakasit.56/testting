const RenderError = (ErrorMsg: string) => (
    <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
      {ErrorMsg}
    </span>
  )
  
  export default RenderError
  