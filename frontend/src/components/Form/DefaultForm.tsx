import { FC, InputHTMLAttributes } from 'react'

import { Field, ErrorMessage } from 'formik'

import RenderError from 'components/Form/RenderError'

interface IFormProps extends InputHTMLAttributes<HTMLInputElement> {
  touched: any
  errors: any
  label: string
  name: string
  placeholder: string
  type: string
  min?: number
}

const DefaultForm: FC<IFormProps> = (props) => {
  const {
    touched,
    errors,
    label,
    name,
    placeholder,
    type,
    min,
  } = props

  return (
    <div className="flex flex-col gap-1">
      <label
        className={`text-s text-left ${touched && errors ? 'text-red-400' : 'text-white'}`}
        htmlFor={name}
      >
        {label}
      </label>
      <Field
        id={name}
        name={name}
        className={`${
          touched && errors
            ? 'focus:bg-red-200 border-red-400 text-red-700'
            : 'focus:bg-gray-200 border-gray-400 text-white'
        } h-12 rounded-xl focus:bg-primary bg-primary px-4 py-2`}
        type={type}
        min={min}
        aria-label={placeholder}
        placeholder={placeholder}
      />
      <ErrorMessage name={name} render={RenderError} />
    </div>
  )
}

DefaultForm.defaultProps = {
  min: 0,
}

export default DefaultForm
