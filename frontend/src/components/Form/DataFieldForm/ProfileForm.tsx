export const EditProfileFieldForm = () => [
  {
    label: "Name",
    name: "name",
    type: "text",
    placeholder: "name",
  },
  {
    label: "Email",
    name: "email",
    type: "email",
    placeholder: "email",
  },
  {
    label: "Country",
    name: "conuntry_code",
    type: "conuntry_code",
    placeholder: "Country",
  },
  {
    label: "Github",
    name: "github_link",
    type: "github_link",
    placeholder: "Github",
  },
  {
    label: "Twitter",
    name: "twitter_link",
    type: "twitter_link",
    placeholder: "Twitter",
  },
  {
    label: "Facebook",
    name: "facebook_link",
    type: "facebook_link",
    placeholder: "Facebook",
  },
];
