export const CreateTeamFieldForm = () => [
    {
      label: 'Team Name',
      name: 'team_name',
      type: 'text',
      placeholder: 'team name',
    },
    {
        label: 'Country',
        name: 'country',
        type: 'country',
        placeholder: 'country',
    },
    {
        label: 'Github',
        name: 'Github',
        type: 'Github',
        placeholder: 'Github',
    },
    {
        label: 'Twitter',
        name: 'Twitter',
        type: 'Twitter',
        placeholder: 'Twitter',
    },
    {
        label: 'Facebook',
        name: 'Facebook',
        type: 'Facebook',
        placeholder: 'Facebook',
    },
  ]

  export const EditTeamFieldForm = () => [
    {
      label: 'Team Name',
      name: 'team_name',
      type: 'text',
      placeholder: 'team name',
    },
    {
        label: 'Country',
        name: 'country',
        type: 'country',
        placeholder: 'country',
    },
    {
        label: 'Github',
        name: 'Github',
        type: 'Github',
        placeholder: 'Github',
    },
    {
        label: 'Twitter',
        name: 'Twitter',
        type: 'Twitter',
        placeholder: 'Twitter',
    },
    {
        label: 'Facebook',
        name: 'Facebook',
        type: 'Facebook',
        placeholder: 'Facebook',
    },
  ]