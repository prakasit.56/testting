export const signUpDataFieldForm = () => [
    {
      label: 'Username',
      name: 'username',
      type: 'text',
      placeholder: 'username',
    },
    {
      label: 'Email',
      name: 'email',
      type: 'email',
      placeholder: 'email',
    },
    {
      label: 'Password',
      name: 'password',
      type: 'password',
      placeholder: 'password',
    },
  ]