import { Formik, Form } from 'formik'
import { FC, InputHTMLAttributes, useState, useMemo } from 'react'
import { RiAccountCircleFill } from "react-icons/ri";
import Select from 'react-select'
import countryList from 'react-select-country-list'

import { Field, ErrorMessage } from 'formik'

import * as validateSchemaForm from 'constants/validateSchemaForm'
import RenderError from 'components/Form/RenderError'

interface IFormProps extends InputHTMLAttributes<HTMLInputElement> {
  touched: any
  errors: any
  label: string
  name: string
  placeholder: string
  type: string
  min?: number
}

const customStyles = {
    valueContainer: (provided, state) => ({
        ...provided,
        borderradius: '8px',
        fontsize: 'small',
        padding: '0.625 rem',
    }),

    control: (styles, state) => ({ 
        ...styles,
        backgroundColor: "#1A2332", 
        boxShadow: state.isFocused ? "#1A2332" : "#1A2332",
        borderColor: state.isFocused ? "#1A2332" : "#1A2332",
        "&:hover": {
            borderColor: state.isFocused ? "white" : "white"
        }
    }),

    option: (provided, state) => ({
        ...provided,
        //   borderBottom: '1px dotted pink',
        //   color: state.isSelected ? 'red' : 'blue',
        padding: 20,
        color: state.isSelected ? 'white':'black',
    }),

    singleValue:(provided) => ({
        ...provided,
        color: 'white',
        padding: '0.625rem',
    }),

    placeholder: (defaultStyles) => {
        return {
            ...defaultStyles,
            color: 'grey',
        }
    },

    input: (base, state) => ({
        ...base,
        color: 'white',
        marginTop: '-2px',
    })
}

const DefaultForm: FC<IFormProps> = (props) => {
  const {
    touched,
    errors,
    label,
    name,
    placeholder,
    type,
    min,
  } = props

  const [value, setValue] = useState('')
  const options = useMemo(() => countryList().getData(), [])

  const changeHandler = value => {
      setValue(value)
  }

  return (
    <div>
        <div className='form px-8 py-4 gap-y-2 rounded-xl'>
            <div className='flex text-3xl'>Create Team</div>

            <div className='flex gap-4 items-end'>
                <RiAccountCircleFill className='w-20 h-20'/> 
                <div className = "flex gap-4 py-2">
                    <button 
                    type="button"
                    className={`w-28 bg-[#0062B9] py-2 hover:bg-white hover:text-[#0062B9]`}>
                        Upload Avatar
                    </button>
                    <button
                    type="button"
                    className={`w-16 bg-[#BF0000] py-2 hover:bg-white hover:text-[#BF0000]`}>
                        Delete
                    </button>
                </div>
            </div>

            <Formik initialValues={{
                name: '',
                country_code: '',
                github_link: '',
                twitter_link: '',
                facebook_link: '',
            }}
            validationSchema={validateSchemaForm.CreateTeamValidationSchema()}
            onSubmit={values => {
                // same shape as initial values
                console.log(values);
            }}>
            {({ errors, touched }) => (
                <Form>
                    <div className="field flex gap-20">
                        <div className='flex-warp grid gap-y-2'>
                            Team Name
                            <Field name="name" type="text" className='px-2'/>
                            <ErrorMessage name="name" />
                        </div>
                        <div className='float-right text-left grid gap-y-2'>
                            Country
                            <Field name="country_code" type="text" className='px-2'/>
                            <ErrorMessage name="country_code" />
                            <Select
                                isSearchable={true}
                                options={options}
                                value={value}
                                onChange={changeHandler}
                                // styles={customStyles}
                                styles={customStyles}
                                placeholder= 'Please Select'
                            />
                        </div>
                    </div>
                    <div className="field  ">
                        <div className='grid gap-y-2'>Github</div>
                        <br/>
                        <Field name="github_link" type="text" className='px-2'/>
                        <ErrorMessage name="github_link" />
                    </div>
                    <div className="field ">
                        <div className='grid gap-y-2'>Twitter</div>
                        <br/>
                        <Field name="twitter_link" type="text" className='px-2'/>
                        <ErrorMessage name="twitter_link" />
                    </div>
                    <div className="field">
                        <div className='grid gap-y-2'>Facebook</div>
                        <br/>
                        <Field name="facebook_link" type="text" className='px-2'/>
                        <ErrorMessage name="facebook_link" />
                    </div>

                    <div className="grid justify-items-end">
                        <button 
                        type="button"
                        className={`w-28 bg-[#0062B9] py-3 hover:bg-white hover:text-[#0062B9] `}>
                            Continue
                        </button>
                    </div>
            </Form>
            )}
            </Formik>
        </div>
      <ErrorMessage name={name} render={RenderError} />
    </div>
  )
}

DefaultForm.defaultProps = {
  min: 0,
}

export default DefaultForm
