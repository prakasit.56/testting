import { atom } from 'recoil'

const loadingAtom = atom({
  key: 'loading',
  default: {
    loading: false,
  },
})

export { loadingAtom }
