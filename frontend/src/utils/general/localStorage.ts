
 /**  * Clear the local storage
  */
 const clearLocal = () => localStorage.clear()
 
 export { clearLocal }
 