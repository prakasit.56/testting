import { FC } from "react";

import { LayoutContent } from 'components/Layouts'

interface IPrivateRouteProps {
  RouteComponent: JSX.Element
}

const PrivateRoute: FC<IPrivateRouteProps> = ({ RouteComponent }) => {

  return <LayoutContent>{RouteComponent}</LayoutContent>
};

export default PrivateRoute;
