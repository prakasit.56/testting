import { FC } from "react";

import { LayoutContent } from 'components/Layouts'

interface IPublicRouteProps {
  RouteComponent: JSX.Element
}

const PublicRoute: FC<IPublicRouteProps> = ({ RouteComponent }) => {

  return <LayoutContent>{RouteComponent}</LayoutContent>
};

export default PublicRoute;
