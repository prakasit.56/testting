const signInMutation = `
  mutation login ($data: UsersLoginInput!) {
    login (data: $data) {
        refresh_token
        access_token
    }
  }`;

const signUpMutation = `
  mutation createUser ($data: UsersCreateInput!) {
    createUser (data: $data) {
        user_id
    }
  }`;

const singOutUserMutation = `
  mutation logout ($data: UsersLogoutInput!) {
    logout (data: $data)
  }`;

const refreshTokenMutation = `
  mutation refreshToken ($data: UserRefreshTokensInput!) {
    refreshToken (data: $data) {
        refresh_token
        access_token
    }
  }`;

const updateUserMutation = `
  mutation updateUser ($data: UsersUpdateInput!, $where: UsersWhereUniqueInput!) {
    updateUser (data: $data, where: $where) {
        user_id
      }
    }`;



export {
  signInMutation,
  signUpMutation,
  refreshTokenMutation,
  singOutUserMutation,
  updateUserMutation,

};
