import { SignUpInterface } from 'interfaces/pages/SignUp'
import { SignInInterface } from 'interfaces/pages/SignIn'
import { UserInterface } from 'interfaces/pages/Users'

import { general } from 'utils'
import { PagesURL } from 'constants/index'

const handleOnSubmitSignUp = (
  SignUp: any,
  value: SignUpInterface,
) => {
  const variables = {
    data: {
      username: value.username,
      password: value.password,
      name: value.username,
      email: value.email,
      user_role: { connect: { user_role_id: "" } }
    },
  }

  /* A way to handle the error. */
  SignUp(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }

    general.http.Goto(PagesURL.SIGN_IN_PAGE)
  })
}

const handleOnSubmitSignIn = (
  SignIn: any,
  value: SignInInterface,
) => {
  const variables = {
    data: {
      username: value.username,
      password: value.password,
    },
  }

  /* A way to handle the error. */
  SignIn(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      if (code === 401) {
        console.log('Please handle log righ here for 401')
        // By pass the setState of the modal which you want to show in here
        /*
        setDialogModalData({
          title: t('error_msg.user.user_dialog_message.login.unexpeted_error'),
          status: 'error',
          show: true,
          submitBtn: general.http.refresh,
        })
        */
        return
      }

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }

    let tokens = res.data.login.refresh_token
    general.auth.setRefreshToken(tokens)

    general.http.Redirect()
  })
}

const handleOnSubmitSignOut = (Logout: any, token: string) => {
  const variables = {
    data: {
      user_id: '',
      refresh_token: token,
    },
  }

  /* A way to handle the error. */
  Logout(variables)

  general.auth.logout()
}

const handleOnSubmitDeleteProfilePicture = (
  Profile: any,
  value: UserInterface,
) => {
  const variables = {
    data: {
      profile_pic: '',
    },
  }

  /* A way to handle the error. */
  Profile(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }
  })
}

const handleOnSubmitProfileSettingSaveChange = (
  Profile: any,
  value: UserInterface,
) => {
  console.log(value)
  const variables = {
    data: {
      name:{
        set : value.name
      },
      email: {set:value.email},
      conuntry_code: {set:value.conuntry_code},
      github_link: {set:value.github_link},
      twitter_link: {set:value.twitter_link},
      facebook_link: {set:value.facebook_link},
    },
    where: {
      user_id: "",
    }
  }
  /* A way to handle the error. */
  Profile(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }
  })
}

export {
  handleOnSubmitSignUp,
  handleOnSubmitSignIn,
  handleOnSubmitSignOut,
  handleOnSubmitDeleteProfilePicture,
  handleOnSubmitProfileSettingSaveChange,
}
