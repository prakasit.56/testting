const createTeamMutation = `
  mutation createTeams ($data: TeamsCreateInput!) {
    createTeams (data: $data) {
        team_id
    }
  }`


export {
    createTeamMutation,
}
