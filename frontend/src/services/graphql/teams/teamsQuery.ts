const getTeam = () => {
  const getgetTeamQuery = `
      query getTeams{
        getTeams {
            team_id
            team_profile_pic
            name
            conuntry_code
            banned
            github_link
            twitter_link
            facebook_link
            teamMembers{
              users {
                name
                profile_pic
              }
              user_id
              status
              type
            }
        }
    }
          `;
  return {
    query: getgetTeamQuery,
    variables: {
      where: {
        team_id: {
          equals: "",
        },
      },
    },
  };
};

const getTeamScore = () => {
  const getgetTeamQuery = `
      query getTeams{
        getTeams {
            team_id
            team_profile_pic
            name
            conuntry_code
            banned
            github_link
            twitter_link
            facebook_link
            teamMembers{
              users {
                name
                profile_pic
                userScores {
                  base_score
                  time_score
                }
              }
              user_id
              status
              type
            }
        }
    }
          `;
  return {
    query: getgetTeamQuery,
    variables: {
      where: {
        team_id: {
          equals: "",
        },
      },
    },
  };
};

export { getTeam, getTeamScore };
