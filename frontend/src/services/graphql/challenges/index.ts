import * as query from 'services/graphql/challenges/challengesQuery'
import * as handler from 'services/graphql/challenges/challengesHandler'
import * as multiplequery from 'services/graphql/challenges/challengeMultipleQuestion'
import * as flagquery from 'services/graphql/challenges/challengeFlagQuestion'
import * as shortquery from 'services/graphql/challenges/challengeShortQuestion'
import * as mutation from 'services/graphql/challenges/challengesMutation'

export { query, handler, multiplequery, flagquery, shortquery, mutation }
