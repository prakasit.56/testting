const getChallenge = () => {
  const getgetChallengeQuery = `
      query getChallenges{
        getChallenges {
            challenge_id
            name
            question
            description
            level_id
            levels {
              name
            }
            types_id
            types {
              name
            }
            categories_id
            scorce
            question
            description
            max_time
        }
    }
          `;

  return {
    query: getgetChallengeQuery,
    variables: {
      where: {
        challenge_id: {
          equals: "",
        },
      },
    },
  };
};

const getFlagChallengeByID = (id: string) => {
  const getgetChallengeQuery = `
      query ($where: ChallengesWhereInput){
        getChallenges (where: $where) {
            challenge_id
            question
            description
            flagQuestions {
              hint
            }
            name
            level_id
            levels {
              name
            }
            types_id
            types {
              name
            }
            scorce
        }
    }
          `;

  return {
    query: getgetChallengeQuery,
    variables: {
      where: {
        challenge_id: {
          equals: id,
        },
      },
    },
  };
};

const getMultipleChallengeByID = (id: string) => {
  const getgetChallengeQuery = `
      query ($where: ChallengesWhereInput){
        getChallenges (where: $where) {
            challenge_id
            question
            description
            multipleQuestions {
              hint
              multipleChoiceQuestion {
                answer
                correct_flag
              }
            }
            name
            level_id
            levels {
              name
            }
            types_id
            types {
              name
            }
            scorce
        }
    }
          `;

  return {
    query: getgetChallengeQuery,
    variables: {
      where: {
        challenge_id: {
          equals: id,
        },
      },
    },
  };
};

const getShortChallengeByID = (id: string) => {
  const getgetChallengeQuery = `
      query ($where: ChallengesWhereInput){
        getChallenges (where: $where) {
            challenge_id
            question
            description
            shortAnswerQuestions {
              hint
            }
            name
            level_id
            levels {
              name
            }
            types_id
            types {
              name
            }
            scorce
        }
    }
          `;

  return {
    query: getgetChallengeQuery,
    variables: {
      where: {
        challenge_id: {
          equals: id,
        },
      },
    },
  };
};

export {
  getChallenge,
  getFlagChallengeByID,
  getMultipleChallengeByID,
  getShortChallengeByID,
};
