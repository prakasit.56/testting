const getFlagChallenge = () => {
    const getgetFlagChallengeQuery = `
      query getChallenges{
        getChallenges {
            challenge_id
            flagQuestions {
                flag_question_id
                challenge_id
                video_link
                description
                question
                answer
                hint
            }
        }
    }
          `

    return {
      query: getgetFlagChallengeQuery,
      variables: {
        where: {
          challenge_id: {
            equals: ""
          }
        }
      },
    }
  }

  
  export { getFlagChallenge }
  