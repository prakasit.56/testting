const getShortChallenge = () => {
    const getgetShortChallengeQuery = `
      query getChallenges{
        getChallenges {
            challenge_id
            shortAnswerQuestions {
                short_answer_question_id
                challenge_id
                question
                hint
                answer
            }
        }
    }
          `

    return {
      query: getgetShortChallengeQuery,
      variables: {
        where: {
          challenge_id: {
            equals: ""
          }
        }
      },
    }
  }

  
  export { getShortChallenge }
  