import * as endpointURL from 'constants/endpointURL'
import * as getEnv from 'constants/getEnv'
import * as PagesURL from 'constants/PagesUrl'
import * as appSettings from 'constants/appSetting'

export {
    endpointURL,
    getEnv,
    PagesURL,
    appSettings
}