import * as Yup from 'yup'
import { FIELD_REQUIRED } from 'constants/validateSchemaForm/errorMessage'

export const CreatFlagChallengeValidationSchema = () =>
  Yup.object().shape({
    name: Yup.string().required(
        FIELD_REQUIRED('Challenge Name is require'),
    ),
    question: Yup.string().required(
      FIELD_REQUIRED('Question is require'),
    ),
    description: Yup.string(),
    video_link: Yup.string(),
    hint: Yup.string().required(
        FIELD_REQUIRED('Hint is require'),
    ),
    answer: Yup.string().required(
      FIELD_REQUIRED('Answer is require'),
    ),
  })

export const CreateMutipleChallengeValidationSchema = () =>
  Yup.object().shape({
    challenge_name: Yup.string().required(
        FIELD_REQUIRED('Challenge Name is require'),
    ),
    question: Yup.string().required(
      FIELD_REQUIRED('Question is require'),
    ),
    hint: Yup.string().required(
        FIELD_REQUIRED('Hint is require'),
    ),
    choice_1: Yup.string().required(
      FIELD_REQUIRED('Choice 1 is require'),
    ),
    choice_2: Yup.string().required(
      FIELD_REQUIRED('Choice 2 is require'),
    ),
    choice_3: Yup.string().required(
        FIELD_REQUIRED('Choice 3 is require'),
    ),
    choice_4: Yup.string().required(
      FIELD_REQUIRED('Choice 4 is require'),
    ),
  })

export const CreateShortAnswerChallengeValidationSchema = () =>
  Yup.object().shape({
    name: Yup.string().required(
      FIELD_REQUIRED('Challenge Name is require'),
    ),
    question: Yup.string().required(
      FIELD_REQUIRED('Question is require'),
    ),
    description: Yup.string().required(
      FIELD_REQUIRED('Description is require'),
    ),
    hint: Yup.string().required(
        FIELD_REQUIRED('Hint is require'),
    ),
    answer: Yup.string().required(
      FIELD_REQUIRED('Answer is require'),
    ),
  })
export const SentFlagChallengeValidationSchema = () =>
  Yup.object().shape({
    username: Yup.string().required(
        FIELD_REQUIRED('Username is require'),
    ),
    email: Yup.string().required(
      FIELD_REQUIRED('Email is require'),
    ),
    password: Yup.string().required(
        FIELD_REQUIRED('Password is require'),
    ),
  })
