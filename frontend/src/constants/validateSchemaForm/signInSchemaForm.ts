import * as Yup from 'yup'
import { FIELD_REQUIRED } from 'constants/validateSchemaForm/errorMessage'

export const SignInUsernameValidationSchema = () =>
  Yup.object().shape({
    username: Yup.string().required(
        FIELD_REQUIRED('Username is require'),
    )
  })

export const SignInPasswordValidationSchema = () =>
  Yup.object().shape({
    password: Yup.string().required(
        FIELD_REQUIRED('Password is require'),
    ),
  })
