// import React, { useState } from 'react';
// import AccordionLayout from './components/TestComponent';

// const Accordion = () => {
//     const [activeIndex, setActiveIndex] = useState(1);

//   return (
//     <div className="accordion" id="accordionExample">
//     <div className="accordion-item bg-white border border-gray-200">
//       <h2 className="accordion-header mb-0" id="headingOne">
//         <button className="
//           accordion-button
//           relative
//           flex
//           items-center
//           w-full
//           py-4
//           px-5
//           text-base text-gray-800 text-left
//           bg-white
//           border-0
//           rounded-none
//           transition
//           focus:outline-none
//         " type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true"
//           aria-controls="collapseOne">
//           Accordion Item #1
//         </button>
//       </h2>
//       <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne"
//         data-bs-parent="#accordionExample">
//         <div className="accordion-body py-4 px-5 text-black">
//           <strong>This is the first item's accordion body.</strong> It is shown by default,
//           until the collapse plugin adds the appropriate classNamees that we use to style each
//           element. These classNamees control the overall appearance, as well as the showing and
//           hiding via CSS transitions. You can modify any of this with custom CSS or overriding
//           our default variables. It's also worth noting that just about any HTML can go within
//           the <code>.accordion-body</code>, though the transition does limit overflow.
//         </div>
//       </div>
//     </div>
//     <div className="accordion-item bg-white border border-gray-200">
//       <h2 className="accordion-header mb-0" id="headingTwo">
//         <button className="
//           accordion-button
//           collapsed
//           relative
//           flex
//           items-center
//           w-full
//           py-4
//           px-5
//           text-base text-gray-800 text-left
//           bg-white
//           border-0
//           rounded-none
//           transition
//           focus:outline-none
//         " type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false"
//           aria-controls="collapseTwo">
//           Accordion Item #2
//         </button>
//       </h2>
//       <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo"
//         data-bs-parent="#accordionExample">
//         <div className="accordion-body py-4 px-5 text-black">
//           <strong>This is the second item's accordion body.</strong> It is hidden by default,
//           until the collapse plugin adds the appropriate classNamees that we use to style each
//           element. These classNamees control the overall appearance, as well as the showing and
//           hiding via CSS transitions. You can modify any of this with custom CSS or overriding
//           our default variables. It's also worth noting that just about any HTML can go within
//           the <code>.accordion-body</code>, though the transition does limit overflow.
//         </div>
//       </div>
//     </div>
//     <div className="accordion-item bg-white border border-gray-200">
//       <h2 className="accordion-header mb-0" id="headingThree">
//         <button className="
//           accordion-button
//           collapsed
//           relative
//           flex
//           items-center
//           w-full
//           py-4
//           px-5
//           text-base text-gray-800 text-left
//           bg-white
//           border-0
//           rounded-none
//           transition
//           focus:outline-none
//         " type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false"
//           aria-controls="collapseThree">
//           Accordion Item #3
//         </button>
//       </h2>
//       <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree"
//         data-bs-parent="#accordionExample">
//         <div className="accordion-body py-4 px-5 text-black">
//           <strong>This is the third item's accordion body.</strong> It is hidden by default,
//           until the collapse plugin adds the appropriate classNamees that we use to style each
//           element. These classNameNamees control the overall appearance, as well as the showing and
//           hiding via CSS transitions. You can modify any of this with custom CSS or overriding
//           our default variables. It's also worth noting that just about any HTML can go within
//           the <code>.accordion-body</code>, though the transition does limit overflow.
//         </div>
//       </div>
//     </div>
//   </div>
//   );
// };
// export default Accordion;

import React from "react";


function Modal() {
  const [showSecondModal, setShowSecondModal] = React.useState(false);
  const [showThirdModal, setShowThirdModal] = React.useState(false);
  const [showFourthModal, setShowFourthModal] = React.useState(false);
  const [showFifthModal, setShowFifthModal] = React.useState(false);
  const [showSixthModal, setShowSixthModal] = React.useState(false);
  const [showSeventhModal, setShowSeventhModal] = React.useState(false);

  const [name, setName] = React.useState('');


  return (
    <div>
      <button
        className="bg-black text-white active:bg-black font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-black"
        type="button"
        onClick={() => setShowSecondModal(true)}
      >
        Test modal
      </button>
      {showSecondModal ? (
        <>
          <div
            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
          >
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                  <h3 className="text-3xl font-semibold text-black">
                    Modal Title
                  </h3>
                  <button
                    className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                    onClick={() => setShowSecondModal(false)}
                  >
                      ×
                  </button>
                </div>
                {/*body*/}
                <div className="flex justify-center">
                  <div className="mb-3 xl:w-10/12">
                    <div className="input-group relative flex flex-wrap items-stretch w-full mb-4 rounded inline">
                      <span className="input-group-text items-center px-3 py-1.5 text-base font-normal text-gray-700 text-center whitespace-nowrap rounded" id="basic-addon2">
                        <button>
                          <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" className="w-4" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
                          </svg>
                        </button>
                      </span>
                      <input type="search" onChange={(e) => setName(e.target.value)} className="form-control relative flex-auto min-w-0 block w-5/12 px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" placeholder="Search" aria-label="Search" aria-describedby="button-addon2"/>
                    </div>
                  </div>
                </div>
                {/*footer*/}
                <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                  <button
                    className="bg-[#0062B9] text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                    type="button"
                    onClick={() => setShowSecondModal(false)}
                  >
                    Back
                  </button>
                  <button
                    className="bg-[#0062B9] text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                    type="button"
                    onClick={() => setShowThirdModal(true)}
                    disabled={!name}
                  >
                    Invite People
                  </button>
                  {showThirdModal ? (
                    <>
                      <div
                        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                      >
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                          {/*content*/}
                          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                            {/*header*/}
                            <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                              <h3 className="text-3xl font-semibold text-black">
                                Caution
                              </h3>
                              <button
                                className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                onClick={() => setShowSecondModal(false)}
                              >
                                  ×
                              </button>
                            </div>
                            {/*body*/}
                            <div className="relative p-6 flex-auto">
                              <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                Your invitation has been sent.
                              </p>
                            </div>
                            {/*footer*/}
                            <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                              <button
                                className="bg-[#0062B9] text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                onClick={() => setShowSecondModal(false)}
                              >
                                Back
                              </button>
                              <button
                                className="bg-[#0062B9] text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                // onClick={() => setShowSecondModal(false)}
                                onClick={() => [setShowSecondModal(false), setShowThirdModal(false)]}
                              >
                                Continue
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </div>
  );
}

export default Modal;
