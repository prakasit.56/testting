import HomePage from 'pages/PrivateRoutes/Home/HomePage'
import LeaderBoardPage from 'pages/PrivateRoutes/Leaderboard/LeaderboardPage'
import TutorialPage from 'pages/PrivateRoutes/TutorialFile/TutorialPage'
import MainChallenge from 'pages/PrivateRoutes/Challenge/MainChallenge'
import PassChallenge from 'pages/PrivateRoutes/Challenge/components/PassChallenge'
import ChallengeMultipleChoice from 'pages/PrivateRoutes/Challenge/ChallengeMultipleChoice'
import ChallengeShortAnswer from 'pages/PrivateRoutes/Challenge/ChallengeShortAnswer'
import FlagChallenge from 'pages/PrivateRoutes/Challenge/FlagChallenge'
import CreateChallenge from 'pages/PrivateRoutes/Challenge/CreateChallenge'
import CreateFlagChallenge from 'pages/PrivateRoutes/Challenge/CreateFlagChallenge'
import CreateMultipleChoice from 'pages/PrivateRoutes/Challenge/CreateMultipleChoice'
import CreateShortAnswer from 'pages/PrivateRoutes/Challenge/CreateShortAnswer'
import MainEvent from 'pages/PrivateRoutes/EventPage/MainEvent'
import PassEvent from 'pages/PrivateRoutes/EventPage/PassEvent'
import ScoreLeaderboard from 'pages/PrivateRoutes/Leaderboard/Single/ScoreLeaderboard'
import TimeScoreLeaderboard from 'pages/PrivateRoutes/Leaderboard/Single/TimeScoreLeaderboard'
import TeamScoreLeaderboard from 'pages/PrivateRoutes/Leaderboard/Team/ScoreLeaderboard'
import Profile from 'pages/PrivateRoutes/Profile/Profile'
import ProfileSetting from 'pages/PrivateRoutes/Profile/ProfileSetting'
import Badge from 'pages/PrivateRoutes/Profile/Badges'
import CreateTeam from 'pages/PrivateRoutes/Team/CreateTeam'
import TeamProfile from 'pages/PrivateRoutes/Team/TeamProfile'
import TeamSetting from 'pages/PrivateRoutes/Team/TeamSetting'
import Course from 'pages/PrivateRoutes/TutorialFile/Course'
import TutorialChallenge from 'pages/PrivateRoutes/TutorialFile/TutorialChallenge'
import ChallengeQuickStart from 'pages/PrivateRoutes/QuickStart/ChallengeQuickStart'
import HomeQuickStart from 'pages/PrivateRoutes/QuickStart/HomeQuickStart'
import LeaderboardQuickStart from 'pages/PrivateRoutes/QuickStart/LeaderboardQuickStart'
import ProfileQuickStart from 'pages/PrivateRoutes/QuickStart/ProfileQuickStart'
import TeamQuickStart from 'pages/PrivateRoutes/QuickStart/TeamQuickStart'
import TutorialQuickStart from 'pages/PrivateRoutes/QuickStart/TutorialQuickStart'

export {
    HomePage,
    LeaderBoardPage,
    TutorialPage,
    MainChallenge,
    PassChallenge,
    ChallengeMultipleChoice,
    ChallengeShortAnswer,
    FlagChallenge,
    CreateChallenge,
    CreateFlagChallenge,
    CreateMultipleChoice,
    CreateShortAnswer,
    MainEvent,
    PassEvent,
    ScoreLeaderboard,
    TimeScoreLeaderboard,
    TeamScoreLeaderboard,
    Profile,
    ProfileSetting,
    Badge,
    CreateTeam,
    TeamProfile,
    TeamSetting,
    Course,
    TutorialChallenge,
    ChallengeQuickStart,
    HomeQuickStart,
    LeaderboardQuickStart,
    ProfileQuickStart,
    TeamQuickStart,
    TutorialQuickStart
}
