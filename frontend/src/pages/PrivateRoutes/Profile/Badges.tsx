import React, { FC } from 'react'
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/solid";
import FeatureCard from "components/Cards/FeatureCard";
import Pagination from './components/PaginationApp';
import Loader from 'components/Loader/Loader'
import { useQuery } from 'urql'
import { badgeAPI } from 'services/graphql';

let test_data
const Badge: FC = () => {
    const [badge] = useQuery(badgeAPI.query.getBadge())
    // console.log(badge.data);

    if (!badge.error && !badge.fetching){
        test_data = badge.data.getUserBadges}
    return (
        !badge.error && !badge.fetching ?(
        
        <div>
            <div className="mt-12 flex gap-5 text-4xl text-white justify-center">Badges</div>
            <div className="mt-12 flex flex-col gap-5">
            <h4 className="text-2xl font-medium tracking-wide text-white text-left">
                My Badges
            </h4>
            <div className="flex gap-10">
                {/* <FeatureCard
                title={badge.data.getUserBadges[0].name}
                description={badge.data.getUserBadges[0].description}
                imgUrl="https://images.unsplash.com/photo-1528109966604-5a6a4a964e8d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
                /> */}
                { badge.data.getUserBadges.map((element)=>{ 
                        // console.log(element)
                        return (
                            <FeatureCard
                            key={element.badge_id}
                            title={element.badges.name}
                            description={element.badges.description}
                            imgUrl="https://images.unsplash.com/photo-1528109966604-5a6a4a964e8d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
                            />
                    )}) }
            </div>
            </div>
            
        </div>
        
        ) : <Loader />
        )
}

export default Badge;