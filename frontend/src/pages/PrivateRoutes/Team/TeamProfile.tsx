import React, { FC } from "react";
import { RiAccountCircleFill } from "react-icons/ri";
import { BiWorld } from "react-icons/bi";
import { AiOutlineTrophy, AiOutlineGithub } from "react-icons/ai";
import { GiArrowScope } from "react-icons/gi";
import { GrFacebook } from "react-icons/gr";
import { BsTwitter } from "react-icons/bs";
import { teamAPI } from "services/graphql";
import { useQuery } from "urql";
import Loader from "components/Loader/Loader";

const TeamProfile: FC = () => {
  const [team] = useQuery(teamAPI.query.getTeam());
  let member;
  let Captain;
  if (!team.error && !team.fetching) {
    console.log(team);
    member = team.data.getTeams[0].teamMembers.filter(
      (e) => e.type === "member"
    );
    Captain = team.data.getTeams[0].teamMembers.filter(
      (e) => e.type === "captain"
    );
  }
  return !team.error && !team.fetching ? (
    <div className="mt-10 flex h-4/5 w-full justify-center text-center text-3xl">
      <form className="form w-7/12 gap-y-2 px-10 py-8">
        <div className="flex items-center justify-between text-3xl">
          Team Profile
        </div>
        <div className="flex justify-between">
          <div className="flex items-start gap-4 text-base">
            <RiAccountCircleFill className="h-24 w-24" />
            <div className="grid gap-y-5 py-4">
              <h6>{team.data.getTeams[0].name}</h6>
              <div className="flex items-center gap-1">
                <BiWorld />
                {team.data.getTeams[0].conuntry_code}
              </div>
            </div>
          </div>
          <button
            type="button"
            className="h-10 w-20 bg-[#0062B9] text-base hover:bg-white hover:text-[#0062B9]"
            onClick={() =>
              (window.location.href =
                "/team-setting" + `?team_id=${team.data.getTeams[0].team_id}`)
            }
          >
            Setting
          </button>
        </div>
        <br />

        <div className="flex justify-start text-3xl">Members</div>

        <div className="flex justify-between py-5">
          <div className="grid gap-5 gap-y-5 px-6 text-base">
            <div className="flex gap-5">
              <a href="#">
                <RiAccountCircleFill className="h-14 w-14" />
              </a>
              <div className="grid gap-y-2 ">
                <div className="flex">{Captain[0].users.name}</div>
                <div className="flex gap-2">
                  <AiOutlineTrophy className="text-2xl" />
                  Team Captain
                </div>
              </div>
            </div>
            {member.map((element) => {
              console.log(member);
              return (
                <div key={element.user_id} className="flex items-center gap-5">
                  <a href="#">
                    <RiAccountCircleFill className="h-14 w-14" />
                  </a>
                  <h6>{element.users.name} </h6>
                </div>
              );
            })}
            {/* <div className='flex gap-5 items-center'>
                        <a href="#"><RiAccountCircleFill className='w-14 h-14'/></a>
                        <h6>ExampleGuy2 </h6>
                    </div>
                    <div className='flex gap-5 items-center'>
                        <a href="#"><RiAccountCircleFill className='w-14 h-14'/></a>
                        <h6>ExampleGuy3 </h6>
                    </div>
                    <div className='flex gap-5 items-center'>
                        <a href="#"><RiAccountCircleFill className='w-14 h-14'/></a>
                        <h6>ExampleGuy4 </h6>
                    </div>
                    <div className='flex gap-5 items-center'>
                        <a href="#"><RiAccountCircleFill className='w-14 h-14'/></a>
                        <h6>ExampleGuy5 </h6>
                    </div> */}
          </div>
          <div className="grid">
            <div className="h-52 w-64 items-start rounded-md bg-zinc-600/[.3] py-20">
              TEAM RANKING
              <div>70 #</div>
            </div>
            <br />
            <div className="flex justify-center gap-x-2 text-xl">
              <GiArrowScope className="text-3xl" />
              30
              <br />
              Scores
            </div>
            <div className="flex gap-x-10 text-5xl">
              <GrFacebook
                onClick={() =>
                  (window.location.href = team.data.getTeams[0].facebook_link)
                }
                style={{ cursor: "pointer" }}
              />
              <AiOutlineGithub 
              onClick={() =>
                (window.location.href = team.data.getTeams[0].github_link)
              }
              style={{ cursor: "pointer" }}/>
              <BsTwitter 
              onClick={() =>
                (window.location.href = team.data.getTeams[0].twitter_link)
              }
              style={{ cursor: "pointer" }}/>
            </div>
          </div>
        </div>
      </form>
    </div>
  ) : (
    <Loader />
  );
};
export default TeamProfile;
