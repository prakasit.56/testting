import { Formik, Form, Field } from 'formik'
import { useMutation } from 'urql'

import React, { FC, useState, useMemo } from 'react'
import { ImCross } from 'react-icons/im'
import { AiOutlineTrophy } from 'react-icons/ai';
import { RiAccountCircleFill } from "react-icons/ri";
import Select from 'react-select'
import countryList from 'react-select-country-list'
import Loader from 'components/Loader/Loader'

import { useQuery } from 'urql'

import { teamAPI } from 'services/graphql';

import { TeamInterface } from 'interfaces/pages/Team';
import * as form from "components/Form"
import * as validateSchemaForm from 'constants/validateSchemaForm'

const customStyles = {
    valueContainer: (provided, state) => ({
        ...provided,
        borderradius: '8px',
        fontsize: 'small',
        padding: '0.625 rem',

    }),

    control: (styles, state) => ({ 
        ...styles,
        height: '45px',
        backgroundColor: "#1A2332", 
        boxShadow: state.isFocused ? "#1A2332" : "#1A2332",
        borderColor: state.isFocused ? "#1A2332" : "#1A2332",
        "&:hover": {
            borderColor: state.isFocused ? "white" : "white"
        }
    }),

    option: (provided, state) => ({
        ...provided,
        //   borderBottom: '1px dotted pink',
        //   color: state.isSelected ? 'red' : 'blue',
        padding: 20,
        color: state.isSelected ? 'white':'black',
    }),

    singleValue:(provided) => ({
        ...provided,
        color: 'white',
        padding: '0.625rem',
    }),

    placeholder: (defaultStyles) => {
        return {
            ...defaultStyles,
            color: 'grey',
        }
    },

    input: (base, state) => ({
        ...base,
        color: 'white',
        marginTop: '-2px',
    })
}

const TeamSetting = (props) => {
    const [showFirstModal, setShowFirstModal] = React.useState(false);
    const [showSecondModal, setShowSecondModal] = React.useState(false);
    const [showThirdModal, setShowThirdModal] = React.useState(false);
    const [showFourthModal, setShowFourthModal] = React.useState(false);
    const [showFifthModal, setShowFifthModal] = React.useState(false);

    const [team] = useQuery(teamAPI.query.getTeam())

    const [name, setName] = React.useState('');

    const [value, setValue] = useState('')
    const options = useMemo(() => countryList().getData(), [])

    const changeHandler = value => {
        setValue(value)
    }

    return (
        !team.error && !team.fetching ? (
        <div className='w-full h-3/5 mt-10 flex justify-center text-2xl text-white'>
            <div className='w-7/12'>
            <form className="form px-8 py-4 gap-y-2 rounded-xl">
                <div className="Team Setting Field">
                <div className='flex'>Setting</div>
                    <div className='flex gap-4 items-end'>
                        <RiAccountCircleFill className='w-20 h-20'/>
                        <div className = "flex gap-4 py-2">
                            <button 
                                type="button"
                                className={`w-28 bg-[#0062B9] py-2 hover:bg-white hover:text-[#0062B9]`}>
                                    Upload Avatar
                            </button>
                            <button
                                // className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                className={`w-16 bg-[#BF0000] py-2 hover:bg-white hover:text-[#BF0000]`}
                                type="button"
                                onClick={() => setShowFirstModal(true)}
                                // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
                            >
                                Delete
                                {/* <i className='mr-2'>
                                    {ButtonIcon}
                                </i> */}
                            </button>
                            {showFirstModal ? (
                                <>
                                <div
                                    className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                                >
                                    <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                    {/*content*/}
                                    <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                        {/*header*/}
                                        <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                        <h3 className="text-3xl font-semibold text-black">
                                            Caution
                                        </h3>
                                        <button
                                            className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                            // onClick={() => setShowSecondModal(false)}
                                            onClick={() => setShowFirstModal(false)}
                                        >
                                            ×
                                        </button>
                                        </div>
                                        {/*body*/}
                                        <div className="relative p-6 flex-auto">
                                        <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                            Are you sure to you want to delete team profile picture?
                                        </p>
                                        </div>
                                        {/*footer*/}
                                        <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                        <button
                                            className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                            type="button"
                                            // onClick={() => setShowSecondModal(false)}
                                            onClick={() => setShowFirstModal(false)}
                                        >
                                            Back
                                        </button>
                                        <button
                                            className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                            type="button"
                                            // onClick={() => setShowSecondModal(false)}
                                            onClick={() => setShowFirstModal(false)}
                                        >
                                            Continue
                                        </button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                                </>
                            ) : null}
                        </div>
                    </div>
                    <div className="field flex gap-20">
                        <div className='flex-warp grid gap-y-2'>
                            Team Name
                            <input type="text"/>
                        </div>
                        <div className='float-right text-left grid gap-y-2'>
                            Country
                            <Select
                                isSearchable={true}
                                options={options}
                                value={value}
                                onChange={changeHandler}
                                // styles={customStyles}
                                styles={customStyles}
                                placeholder= 'Please Select'
                            />
                        </div>
                    </div>
                    <div className="field  ">
                        <div className='grid gap-y-2'>Github
                        <br/><input type="text"/></div>
                    </div>
                    <div className="field ">
                        <div className='grid gap-y-2'>Twitter
                        <br/><input type="text"/></div>
                    </div>
                    <div className="field">
                        <div className='grid gap-y-2'>Facebook
                        <br/><input type="text"/></div>
                    </div>
                </div>
            </form>
            <br/>
            <div>
                <div className='flex justify-between items-end py-4'>
                    Members
                <button
                    // className="bg-[#0062B9] text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                    className='w-28 bg-[#0062B9] py-2 hover:bg-white hover:text-[#0062B9]'
                    type="button"
                    onClick={() => setShowSecondModal(true)}
                >
                    Invite People
                </button>
                </div>
                {showSecondModal ? (
                    <>
                    <div
                        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                        {/*content*/}
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                            {/*header*/}
                            <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                            <h3 className="text-3xl font-semibold text-black">
                                Invite People
                            </h3>
                            <button
                                className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                onClick={() => setShowSecondModal(false)}
                            >
                                ×
                            </button>
                            </div>
                            {/*body*/}
                            <div className="flex justify-center">
                            <div className="mb-3 xl:w-10/12">
                                <div className="input-group relative flex flex-wrap items-stretch w-full mb-4 rounded inline">
                                <span className="input-group-text items-center px-3 py-1.5 text-base font-normal text-gray-700 text-center whitespace-nowrap rounded" id="basic-addon2">
                                    <button>
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" className="w-4" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
                                    </svg>
                                    </button>
                                </span>
                                <input type="search" onChange={(e) => setName(e.target.value)} className="form-control relative flex-auto min-w-0 block w-5/12 px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" placeholder="Search" aria-label="Search" aria-describedby="button-addon2"/>
                                </div>
                            </div>
                            </div>
                            {/*footer*/}
                            <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                            <button
                                className="bg-[#0062B9] text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                onClick={() => setShowSecondModal(false)}
                            >
                                Back
                            </button>
                            <button
                                className="bg-[#0062B9] text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                onClick={() => setShowThirdModal(true)}
                                disabled={!name}
                            >
                                Invite People
                            </button>
                            {showThirdModal ? (
                                <>
                                <div
                                    className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                                >
                                    <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                    {/*content*/}
                                    <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                        {/*header*/}
                                        <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                        <h3 className="text-3xl font-semibold text-black">
                                            Caution
                                        </h3>
                                        <button
                                            className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                            onClick={() => setShowThirdModal(false)}
                                        >
                                            ×
                                        </button>
                                        </div>
                                        {/*body*/}
                                        <div className="relative p-6 flex-auto">
                                        <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                            Your invitation has been sent.
                                        </p>
                                        </div>
                                        {/*footer*/}
                                        <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                        <button
                                            className="bg-[#0062B9] text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                            type="button"
                                            onClick={() => setShowThirdModal(false)}
                                        >
                                            Back
                                        </button>
                                        <button
                                            className="bg-[#0062B9] text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                            type="button"
                                            // onClick={() => setShowSecondModal(false)}
                                            onClick={() => [setShowSecondModal(false), setShowThirdModal(false)]}
                                        >
                                            Continue
                                        </button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                                </>
                            ) : null}
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                ) : null}
            </div>
            <div>
                <div className='text-base'>
                        <div className='h-20 w-8/10 flex bg-[#211A3A] rounded mb-1 items-center justify-between'>
                            <div className='flex items-center gap-3 px-5'>
                                <RiAccountCircleFill className='h-16 w-16'/>
                                <a href="#" className='grid gap-y-2 '>
                                    <div className='flex not-italic font-bold leading-7 text-yellow-600'>
                                        ExampleGuy 1
                                    </div>
                                    <div className='flex gap-2 text-sm items-center'>
                                        <AiOutlineTrophy className='text-xl'/>
                                        Team Captain
                                    </div>
                                </a>
                            </div>                    
                            <a href="#" className='item-center px-8' onClick={() => setShowFifthModal(true)}><ImCross/></a>
                        </div>
                        <div className='h-20 w-8/10 flex bg-zinc-600 rounded mb-1 items-center justify-between'>
                            <div className='flex items-center gap-3 px-5'>
                                <RiAccountCircleFill className='h-16 w-16'/>
                                <a href="#">
                                    <h6 className='h-6 not-italic font-bold leading-7 text-yellow-600'>
                                        ExampleGuy 2</h6></a>
                            </div>
                            <a href="#" className='item-center px-8' onClick={() => setShowFifthModal(true)}><ImCross/></a>
                        </div>
                        <div className='h-20 w-8/10 flex bg-[#211A3A] rounded mb-1 items-center justify-between'>
                            <div className='flex items-center gap-3 px-5'>
                                <RiAccountCircleFill className='h-16 w-16'/>
                                <a href="#">
                                    <h6 className='h-6 not-italic font-bold leading-7 text-yellow-600'>
                                        ExampleGuy 3</h6></a>
                            </div>
                            <a href="#" className='item-center px-8' onClick={() => setShowFifthModal(true)}><ImCross/></a>
                        </div>
                        <div className='h-20 w-8/10 flex bg-zinc-600 rounded mb-1 items-center justify-between'>
                            <div className='flex items-center gap-3 px-5'>
                                <RiAccountCircleFill className='h-16 w-16'/>
                                <a href="#">
                                    <h6 className='h-6 not-italic font-bold leading-7 text-yellow-600'>
                                        ExampleGuy 4</h6></a>
                            </div>
                            <a href="#" className='item-center px-8' onClick={() => setShowFifthModal(true)}><ImCross/></a>
                        </div>
                        <div className='h-20 w-8/10 flex bg-[#211A3A] rounded mb-1 items-center justify-between'>
                            <div className='flex items-center gap-3 px-5'>
                                <RiAccountCircleFill className='h-16 w-16'/>
                                <a href="#">
                                    <h6 className='h-6 not-italic font-bold leading-7 text-yellow-600'>
                                        ExampleGuy 5</h6></a>
                            </div>
                            <a href="#" className='item-center px-8' onClick={() => setShowFifthModal(true)}><ImCross/></a>
                        </div>
                    </div>          
                </div>
                <br/>
                <div className="grid justify-items-end">
                    <button
                        // className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                        className={`w-28 bg-[#0062B9] py-2 hover:bg-white hover:text-[#0062B9] `}
                        type="button"
                        onClick={() => setShowFourthModal(true)}
                        // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
                    >
                        Save Changes
                        {/* <i className='mr-2'>
                            {ButtonIcon}
                        </i> */}
                    </button>
                </div>
                {showFourthModal ? (
                    <>
                    <div
                        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                        {/*content*/}
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                            {/*header*/}
                            <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                            <h3 className="text-3xl font-semibold text-black">
                                Confirm
                            </h3>
                            <button
                                className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                // onClick={() => setShowSecondModal(false)}
                                onClick={() => setShowFourthModal(false)}
                            >
                                ×
                            </button>
                            </div>
                            {/*body*/}
                            <div className="relative p-6 flex-auto">
                            <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                Your data has been saved
                            </p>
                            </div>
                            {/*footer*/}
                            <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                            <button
                                className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                // onClick={() => setShowSecondModal(false)}
                                onClick={() => setShowFourthModal(false)}
                            >
                                Continue
                            </button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                ) : null}
                {showFifthModal ? (
                    <>
                    <div
                        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                        {/*content*/}
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                            {/*header*/}
                            <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                            <h3 className="text-3xl font-semibold text-black">
                                Caution
                            </h3>
                            <button
                                className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                // onClick={() => setShowSecondModal(false)}
                                onClick={() => setShowFifthModal(false)}
                            >
                                ×
                            </button>
                            </div>
                            {/*body*/}
                            <div className="relative p-6 flex-auto">
                            <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                Are you sure to you want to remove this people from your team?
                            </p>
                            </div>
                            {/*footer*/}
                            <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                            <button
                                className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                // onClick={() => setShowSecondModal(false)}
                                onClick={() => setShowFifthModal(false)}
                            >
                                Back
                            </button>
                            <button
                                className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                // onClick={() => setShowSecondModal(false)}
                                onClick={() => setShowFifthModal(false)}
                            >
                                Continue
                            </button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                ) : null}
            </div>
        </div>
        ) : <Loader />
    )
}
export default TeamSetting
