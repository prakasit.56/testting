import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/solid"
import FeatureCard from "components/Cards/FeatureCard"
import { useQuery } from "urql"
import { tutorialAPI } from "services/graphql"
import Loader from "components/Loader/Loader"

type Props = {}
let percent = "50%"
function percentChapter(curr, total) {
  percent = `${((curr / total) * 100).toFixed(2)}%`
}
let courseIndex = 0
function TutorialPage({}: Props) {
  const [UserTutorial] = useQuery(tutorialAPI.query.getUserTutorial())
  const [tutorial] = useQuery(tutorialAPI.query.getTutorial())

  let tempUserTutorialCurr
  let UserTutorialFilter
  if (!UserTutorial.error && !UserTutorial.fetching) {
    UserTutorialFilter = UserTutorial.data.getUserTutorials.filter(
      (value, index, self) =>
        index ===
        self.findIndex(
          (t) =>
            t.tutorialChapterDatas.tutorial_id ===
            value.tutorialChapterDatas.tutorial_id
        )
    )
    if (!tutorial.error && !tutorial.fetching) {
      courseIndex = tutorial.data.getTutorialContents.findIndex(
        (t) =>
          t.tutorial_id ===
          UserTutorialFilter[UserTutorialFilter.length-1].tutorialChapterDatas.tutorial_id
      )
      tempUserTutorialCurr = UserTutorial.data.getUserTutorials.filter(
        (e) =>
          e.tutorialChapterDatas.tutorial_id ===
          tutorial.data.getTutorialContents[courseIndex].tutorial_id
      )
      percentChapter(
        tempUserTutorialCurr.length,
        tutorial.data.getTutorialContents[courseIndex].tutorialChapterDatas.length
      )
    }
  }

  return !UserTutorial.error &&
    !UserTutorial.fetching &&
    !tutorial.error &&
    !tutorial.fetching ? (
    <>
      <div className="mt-10 flex min-h-screen flex-col gap-4">
        <h1 className="text-4xl font-bold text-white text-center">Tutorial</h1>
        <div className="flex w-full flex-col gap-3 px-10">
          <h4 className="text-2xl  font-medium tracking-wide text-white">
            My progress
          </h4>
          <div className="grid h-44 w-full grid-cols-5 grid-rows-1 overflow-hidden rounded-2xl bg-primary">
            <div className="col-span-1 w-64">
              <img
                src="https://images.unsplash.com/photo-1528109966604-5a6a4a964e8d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
                alt="coding for life"
                className=""
              />
            </div>
            <div className="col-span-3 flex flex-col items-start justify-start gap-5 py-2 px-2 mt-2 text-white">
              <h2 className="text-2xl font-medium">
                My COURSES :{" "}
                {tutorial.data.getTutorialContents[courseIndex].name}
              </h2>
              <h6>
                Chapter {tempUserTutorialCurr.length} : {tempUserTutorialCurr[tempUserTutorialCurr.length - 1].tutorialChapterDatas.chapter_name}
              </h6>
              <div className="flex w-full items-center gap-4">
                <h6>Progress :</h6>
                <div className="h-3 w-96 overflow-hidden rounded-full border border-[#37B8F0] bg-primary">
                  <div
                    style={{ width: percent }}
                    className="h-3 rounded-full bg-[#37B8F0]"
                  ></div>
                </div>
                <h6 className="text-[#0FB1D9]">{percent}</h6>
              </div>
            </div>

            <div className="flex flex-col items-end justify-between p-4 text-white">
              <button
                type="button"
                className={`w-56 rounded-xl border border-white bg-[#344663] py-3 px-4 text-3xl font-semibold text-[#FEC751] shadow-md`}
              >
                <h3 className="text-sm font-light leading-4 tracking-wide">
                  Continue to your course <br/>ID { tutorial.data.getTutorialContents[courseIndex].tutorial_id }
                </h3>
              </button>
              {/* <div className="flex gap-4">
                <button
                  type="button"
                  className={`inline-flex w-40 items-center justify-center rounded-xl bg-[#0062B9] py-3 px-4 text-3xl font-semibold text-white shadow-md`}
                >
                  <ChevronLeftIcon className="h-5 w-5" />
                  <h3 className="text-sm font-light leading-4 tracking-wide">
                    Previous Course
                  </h3>
                </button>
                <button
                  type="button"
                  className={`inline-flex w-40 items-center justify-center rounded-xl bg-[#0062B9] py-3 px-4 text-3xl font-semibold text-white shadow-md`}
                >
                  <h3 className="text-sm font-light leading-4 tracking-wide">
                    Next Course
                  </h3>
                  <ChevronRightIcon className="h-5 w-5" />
                </button>
              </div> */}
            </div>
          </div>
        </div>

        <div className="mt-12 flex flex-col gap-5 justify-start px-10">
          <h4 className="text-2xl font-medium tracking-wide text-white">
            My Course
          </h4>
          <div className="flex gap-10">
            {tutorial.data.getTutorialContents.map((element) => {
              return (
                <FeatureCard
                  key={ element.tutorial_id }
                  title={ element.name }
                  imgUrl="https://images.unsplash.com/photo-1528109966604-5a6a4a964e8d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
                />
              )
            })}
          </div>
          {/* <div className="flex w-full justify-end">
            <div className="h-16 w-96 rounded-2xl bg-primary"></div>
          </div> */}
        </div>
      </div>
    </>
  ) : (
    <Loader />
  )
}

export default TutorialPage
