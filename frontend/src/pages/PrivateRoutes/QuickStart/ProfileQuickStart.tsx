import React, { FC } from 'react';
import Profile from "pages/PrivateRoutes/Profile/Profile";

const ProfileQuickStart: FC = () => {
    return (
        <div className='cursor-default bg-black opacity-70 fixed inset-0 w-full h-full'>
            <div className='py-20'>
                <div className='justify-center'>Click here to edit your account or logout from your account.</div>
                <br/>
                <div className='flex gap-5 justify-center'>
                <a href="/challenge-quickstart"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Previous</button></a>
                <a href="/team-quickstart"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Next</button></a>
                <a href="/main-events"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Skip</button></a>
                </div>
                <div className='opacity-20 grid place-items-center '>
                <Profile />
            </div>
            </div>
            
        </div>
        )
}
export default ProfileQuickStart
