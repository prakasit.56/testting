import React, { FC } from 'react';
import TutorialPage from 'pages/PrivateRoutes/TutorialFile/TutorialPage';

const TutorialQuickStart: FC = () => {
    return (
        <div className='cursor-default bg-black opacity-50 fixed inset-0 w-full h-full'>
            <div className='py-20'>
                <div className='justify-center'>Click here to start tutorial. You can learn the basic of security from this feature.</div>
                <div className='flex gap-5 justify-center'>
                    <a href="/home-quickstart"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Previous</button></a>
                    <a href="/leaderboard-quickstart"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Next</button></a>
                    <a href="/main-events"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Skip</button></a>
                </div>

            </div>
            <div className='grid place-items-center opacity-20'>
                <TutorialPage />
            </div>
            
        </div>
        )
}
export default TutorialQuickStart
