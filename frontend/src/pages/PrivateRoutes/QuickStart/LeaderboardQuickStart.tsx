import React, { FC } from 'react';
import ScoreLeaderboard from "pages/PrivateRoutes/Leaderboard/Single/ScoreLeaderboard";

const LeaderboardQuickStart: FC = () => {
    return (
        <div className='cursor-default bg-black opacity-70 fixed inset-0 w-full h-full'>
            <div className='py-20'>
                <div className='justify-center'>Click here to start Leaderboard. You can know who has the highest score or highest team score.</div>
                <br/>
                <div className='flex gap-5 justify-center'>
                    <a href="/tutorial-quickstart"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Previous</button></a>
                    <a href="/challenge-quickstart"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Next</button></a>
                    <a href="/main-events"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Skip</button></a>
                </div>
                <div className='opacity-20'>
                    <ScoreLeaderboard />
                </div>
            </div>
            
        </div>
        )
}
export default LeaderboardQuickStart
