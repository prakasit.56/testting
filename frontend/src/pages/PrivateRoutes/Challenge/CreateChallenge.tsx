import React, { FC, useState } from 'react'
import { DotLeft, DotRight } from "components/Dot";
import Select from 'react-select';

const CreateChallenge: FC = () => {

    const [selectedOption, setSelectedOption] = useState(null);

    let link
    const ChangeToPage = (event) => {
        link = event.value
        console.log(event.value)
    };

    const ChangePage = (event) => {
        window.location.href='/' + (link);
    };

    const options = [
        { value: 'create-challenge-short-answer', label: 'Short Answer' },
        { value: 'create-challenge-multiple-choice', label: 'Multiple Choice' },
        { value: 'create-flag-challenge"', label: 'Flag Challenge' },
    ];

    const customStyles = {
        control: (styles, state) => ({
            ...styles,
            backgroundColor: "#0062B9",
            boxShadow: state.isFocused ? "#0062B9" : "#0062B9",
            borderColor: state.isFocused ? "#0062B9" : "#0062B9",
            "&:hover": {
                borderColor: state.isFocused ? "white" : "white",
            },
        }),

        singleValue: (provided) => ({
            ...provided,
            color: "white"
        }),

        placeholder: (defaultStyles) => {
            return {
                ...defaultStyles,
                color: "white",
            };
        },

        input: (base, state) => ({
            ...base,
            color: "white",
          }),
    }

    return (
    <div className='w-full h-screen pb-20 flex justify-center m-auto items-center'>
    <div className='w-6/12 px-8 py-10 gap-y-2 text-center text-white'>
        <div className='text-3xl text-bold text-sky-300'> Create Challenge </div>
        <br/><br/>
        <div>
            <div className='text-3xl flex-center'>Challenge Type</div>
            <br/>
            <div className="relative p-10 items-center">
            <DotRight />
                {/* <select defaultValue={"#"} className='bg-[#0062B9] w-9/12 h-20 text-xl text-center rounded-md' onChange={ChangeToPage}>
                    <option value="#" disabled selected>Select Option...</option>
                    <option value="create-challenge-short-answer">Short Answer</option>
                    <option value="create-challenge-multiple-choice">Multiple Choice</option>
                    <option value="create-flag-challenge">Flag Challenge</option>
                </select> */}
                <Select
                    defaultValue={selectedOption}
                    onChange={setSelectedOption && ChangeToPage}
                    options={options}
                    className="text-black"
                    styles={customStyles}
                    placeholder="Select Option..."
                />
            <DotLeft />
            </div>
            <div className='w-full flex px-10 justify-end'>
                <button onClick={ChangePage} className='w-24 h-12 bg-[#0062B9] hover:bg-white hover:text-[#0062B9]'>Continue</button>
            </div>
        </div>         
    </div></div>
    )
}
export default CreateChallenge
