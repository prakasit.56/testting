import React, { FC } from 'react'
import { TiTick } from 'react-icons/ti'

const PassEvent: FC = () => {
    return (
        <div className='w-7/12'>
            <div className='flex justify-center'>
            {/* <StarLeft /> */}
            <div className='bg-[#ffd233] rounded-[20px] w-2/4 text-black py-5'>
                <TiTick style={{fontSize: '100px', color: 'black'}} className='w-full justify-center'/>
                <div>Correct Answer</div>
                <div>100 Points Gained</div>
            </div>
            </div>
            <br/>
            <div className='text-lg grid justify-items-center'>
                <div className='flex gap-2'>Challenge Points : <div className='text-yellow-400'>80 pts</div> <div>(10/10)</div></div>
                <div className='flex gap-2'>Extra Points : <div className='text-yellow-400'>20 pts</div> <div>(From completing the challenge in 15 minutes)</div></div>
            </div>
            {/* <StarRight /> */}
            <br/>
            <button className='w-36 h-12 bg-[#0062B9] hover:text-[#0062B9] hover:bg-white text-base'>Continue</button>
           
        </div>
        )
}
export default PassEvent
