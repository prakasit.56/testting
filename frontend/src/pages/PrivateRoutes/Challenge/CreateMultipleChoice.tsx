import { Formik, Form } from 'formik'
import { useMutation } from 'urql'

import React, { FC } from 'react'

import * as form from "components/Form"
import { challengeAPI } from 'services/graphql';
import { CreateFlagChallengeDataInterface } from 'interfaces/pages/Challenge'
import * as validateSchemaForm from 'constants/validateSchemaForm'

const CreateMultipleChoice = (props) => {
    const [showFirstModal, setShowFirstModal] = React.useState(false);
    const [showSecondModal, setShowSecondModal] = React.useState(false);
    const [showThirdModal, setShowThirdModal] = React.useState(false);

    const ChangeToPage = (event) => {
        window.location.href='/' + (event.target.value);
    };

    return (
        <div className='form w-8/12 flex px-12 py-8 text-lg pb-14'>
            <div className='grid text-3xl'>
                <div>Create Challenge</div>
                <div className='text-[#0FB1D9]'>Multiple Choice</div>
            </div>
            <br/><br/>
            <div className='grid gap-y-2 '>
                <div className='flex'>Challenge Name</div>
                <input 
                id="challengeName"
                type="text"
                className='w-5/12 ' 
                />
            </div>
            <br/>
            <div className='flex gap-20 justify-between'>
                <div className='w-9/12 grid gap-y-4 text-left'>
                    <div>
                        <div>Question</div>
                        <input 
                            id="Question"
                            type="text"
                        />
                    </div>
                    <div>
                        <div>Hint</div>
                        <input 
                            id="Hint"
                            type="text"
                        />
                    </div>
                    <div>
                        <div>Choice</div>
                        <div className='inline-flex w-full gap-x-3'>
                            <input 
                                id="Choice-1"
                                type="text"
                            />
                           <div className="form-check inline-flex items-center">
                                <input className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="checkbox" value="true" id="flexCheckChecked" />
                                <label className="form-check-label inline-block text-white content-center">
                                    Correct
                                </label>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>Choice</div>
                        <div className='inline-flex w-full gap-x-3'>
                            <input 
                                id="Choice-2"
                                type="text"
                            />
                            <div className="form-check inline-flex items-center">
                                <input className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="checkbox" value="true" id="flexCheckChecked" />
                                <label className="form-check-label inline-block text-white content-center">
                                    Correct
                                </label>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>Choice</div>
                        <div className='inline-flex w-full gap-x-3'>
                            <input 
                                id="Choice-3"
                                type="text"
                            />
                            <div className="form-check inline-flex items-center">
                                <input className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="checkbox" value="true" id="flexCheckChecked" />
                                <label className="form-check-label inline-block text-white content-center">
                                    Correct
                                </label>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>Choice</div>
                        <div className='inline-flex w-full gap-x-3'>
                            <input 
                                id="Choice-4"
                                type="text"
                            />
                            <div className="form-check inline-flex items-center">
                                <input className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="checkbox" value="true" id="flexCheckChecked" />
                                <label className="form-check-label inline-block text-white content-center">
                                    Correct
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>

            <div className='flex gap-3'>
                <button onClick={() => window.location.href='/create-challenge'} className='w-28 bg-[#0062B9] py-2 hover:bg-white hover:text-[#0062B9]'>
                    Back
                </button>
                <button
                    className="w-32 bg-[#166615] py-2 hover:bg-white hover:text-[#166615]"
                    type="button"
                    onClick={() => setShowThirdModal(true)}
                    // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
                >
                    Submit
                {/* <i className='mr-2'>
                {ButtonIcon}
                </i> */}
                </button>
            </div>
            <br/><br/>
                {showThirdModal ? (
                    <>
                    <div
                        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                        {/*content*/}
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                            {/*header*/}
                            <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                            <h3 className="text-3xl font-semibold text-black">
                                Confirm
                            </h3>
                            <button
                                className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                // onClick={() => setShowSecondModal(false)}
                                onClick={() => setShowThirdModal(false)}
                            >
                                ×
                            </button>
                            </div>
                            {/*body*/}
                            <div className="relative p-6 flex-auto">
                            <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                Are you sure you want to send your challenge to admin  ?
                            </p>
                            </div>
                            {/*footer*/}
                            <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                            <button
                                className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                // onClick={() => setShowSecondModal(false)}
                                onClick={() => setShowThirdModal(false)}
                            >
                                Back
                            </button>
                            <button
                                className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                // onClick={() => setShowSecondModal(false)}
                                onClick={() => setShowThirdModal(false)}
                            >
                                Continue
                            </button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                ) : null}
                
                {showFirstModal ? (
                    <>
                    <div
                        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                        {/*content*/}
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                            {/*header*/}
                            <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                            <h3 className="text-3xl font-semibold text-black">
                                Caution
                            </h3>
                            <button
                                className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                onClick={() => setShowFirstModal(false)}
                            >
                                ×
                            </button>
                            </div>
                            {/*body*/}
                            <div className="relative p-6 flex-auto">
                            <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                Are you sure to you want to delete this question?
                            </p>
                            </div>
                            {/*footer*/}
                            <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                            <button
                                className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                onClick={() => setShowFirstModal(false)}
                            >
                                Back
                            </button>
                            <button
                                className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                onClick={() => setShowSecondModal(true)}
                                // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
                            >
                                Continue
                                {/* <i className='mr-2'>
                                    {ButtonIcon}
                                </i> */}
                            </button>
                            {showSecondModal ? (
                                <>
                                <div
                                    className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                                >
                                    <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                    {/*content*/}
                                    <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                        {/*header*/}
                                        <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                        <h3 className="text-3xl font-semibold text-black">
                                            Deleted
                                        </h3>
                                        <button
                                            className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                            // onClick={() => setShowSecondModal(false)}
                                            onClick={() => [setShowSecondModal(false), setShowFirstModal(false)]}
                                        >
                                            ×
                                        </button>
                                        </div>
                                        {/*body*/}
                                        <div className="relative p-6 flex-auto">
                                        <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                            Your Question has been deleted.
                                        </p>
                                        </div>
                                        {/*footer*/}
                                        <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                        <button
                                            className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                            type="button"
                                            // onClick={() => setShowSecondModal(false)}
                                            onClick={() => [setShowSecondModal(false), setShowFirstModal(false)]}
                                        >
                                            Continue
                                        </button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                                </>
                            ) : null}
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                ) : null}
            
            
        </div>
    
    )
}
export default CreateMultipleChoice
