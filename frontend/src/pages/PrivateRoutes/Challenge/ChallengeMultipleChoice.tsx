import React, { FC } from "react";
import { useLocation } from "react-router-dom";
import Loader from "components/Loader/Loader";

import { useQuery } from "urql";

import { challengeAPI } from "services/graphql";

const ChallengeMultipleChoice: FC = () => {
  const [showFirstModal, setShowFirstModal] = React.useState(false);
  const [showSecondModal, setShowSecondModal] = React.useState(false);
  const [showThirdModal, setShowThirdModal] = React.useState(false);

  const search = useLocation().search;
  const challenge_id = new URLSearchParams(search).get("challenge_id");
  const [challenge_by_id] = useQuery(
    challengeAPI.query.getMultipleChallengeByID(challenge_id)
  );

  return !challenge_by_id.error && !challenge_by_id.fetching ? (
    <div className="mt-10 flex w-full items-center justify-center">
      <div className="w-7/12 gap-y-2 px-10 py-8 text-center">
        <div className="flex justify-end">
          <div className="flex h-10 w-52 justify-center gap-x-3 bg-[#455D84]/[.5] py-2 px-2">
            <div className="text-white">INSTRUCTIONS</div>
            <div className="text-[#FEC751]">
              {challenge_by_id.data.getChallenges[0].scorce}
            </div>
          </div>
        </div>
        <br />
        <div className="text-left text-white">
          <div className="text-2xl text-[#0FB1D9]">
            {challenge_by_id.data.getChallenges[0].name}
          </div>
          <div className="text-lg">
            {challenge_by_id.data.getChallenges[0].question}
          </div>
          <div className="text-lg">
            {challenge_by_id.data.getChallenges[0].description}
          </div>
        </div>
        <br />
        <div className="ml-8 block text-lg font-medium text-white">
          <div className="mb-4 flex items-center gap-2">
            <input
              id="challenge-option-1"
              type="radio"
              name="choice"
              value="1"
              className="h-4 w-4 border-gray-300 focus:ring-2 focus:ring-blue-300"
              aria-labelledby="challenge-option-1"
              aria-describedby="challenge-option-1"
            />
            <label
              htmlFor="challenge-option-1"
              className="ml-2 block text-sm font-medium text-white"
            >
              {
                challenge_by_id.data.getChallenges[0].multipleQuestions[0]
                  .multipleChoiceQuestion[0].answer
              }
            </label>
          </div>
          <div className="mb-4 flex items-center gap-2">
            <input
              id="challenge-option-2"
              type="radio"
              name="choice"
              value="2"
              className="h-4 w-4 border-gray-300 focus:ring-2 focus:ring-blue-300"
              aria-labelledby="challenge-option-2"
              aria-describedby="challenge-option-2"
            />
            <label
              htmlFor="challenge-option-2"
              className="ml-2 block text-sm font-medium text-white"
            >
              {
                challenge_by_id.data.getChallenges[0].multipleQuestions[0]
                  .multipleChoiceQuestion[1].answer
              }
            </label>
          </div>
          <div className="mb-4 flex items-center gap-2">
            <input
              id="challenge-option-3"
              type="radio"
              name="choice"
              value="3"
              className="h-4 w-4 border-gray-300 focus:ring-2 focus:ring-blue-300"
              aria-labelledby="challenge-option-3"
              aria-describedby="challenge-option-3"
            />
            <label
              htmlFor="challenge-option-3"
              className="ml-2 block text-sm font-medium text-white"
            >
              {
                challenge_by_id.data.getChallenges[0].multipleQuestions[0]
                  .multipleChoiceQuestion[2].answer
              }
            </label>
          </div>
          <div className="mb-4 flex items-center gap-2">
            <input
              id="challenge-option-4"
              type="radio"
              name="choice"
              value="4"
              className="h-4 w-4 border-gray-300 focus:ring-2 focus:ring-blue-300"
              aria-labelledby="challenge-option-4"
              aria-describedby="challenge-option-4"
            />
            <label
              htmlFor="challenge-option-4"
              className="ml-2 block text-sm font-medium text-white"
            >
              {
                challenge_by_id.data.getChallenges[0].multipleQuestions[0]
                  .multipleChoiceQuestion[3].answer
              }
            </label>
          </div>
        </div>
        <br />
        <div className="flex h-16 items-center justify-between rounded-md bg-[#455D84]/[.5] px-2">
          <div className="flex h-12 gap-2">
            <button className="w-36 bg-[#0062B9] hover:bg-white hover:text-[#0062B9]">
              Previous
            </button>
            <button className="w-36 bg-[#0062B9] hover:bg-white hover:text-[#0062B9]">
              Next
            </button>
          </div>
          <button
            className="h-10 w-48 bg-[#344663] px-5 text-yellow-400 outline outline-white"
            type="button"
            onClick={() => setShowThirdModal(true)}
            // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
          >
            Take Hint
            <div className="display: inline">(-15 points)</div>
            {/* <i className='mr-2'>
                                {ButtonIcon}
                            </i> */}
          </button>

          {showThirdModal ? (
            <>
              <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
                <div className="relative my-6 mx-auto w-auto max-w-3xl">
                  {/*content*/}
                  <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                    {/*header*/}
                    <div className="flex items-start justify-between rounded-t border-b border-solid border-slate-200 p-5">
                      <h3 className="text-3xl font-semibold text-black">
                        Hint
                      </h3>
                      <button
                        className="w-10 place-content-center rounded-full text-4xl text-[#FF0505] transition-all duration-150 hover:text-red-800"
                        // onClick={() => setShowSecondModal(false)}
                        onClick={() => setShowThirdModal(false)}
                      >
                        ×
                      </button>
                    </div>
                    {/*body*/}
                    <div className="relative flex-auto p-6">
                      <p className="my-4 text-lg leading-relaxed text-slate-500 text-[#0062B9]">
                        {
                          challenge_by_id.data.getChallenges[0]
                            .multipleQuestions[0].hint
                        }
                      </p>
                    </div>
                    {/*footer*/}
                    <div className="flex items-center justify-end rounded-b border-t border-solid border-slate-200 p-6">
                      <button
                        className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                        type="button"
                        // onClick={() => setShowSecondModal(false)}
                        onClick={() => setShowThirdModal(false)}
                      >
                        Continue
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="fixed inset-0 z-40 bg-black opacity-25"></div>
            </>
          ) : null}
        </div>
      </div>
    </div>
  ) : (
    <Loader />
  );
};
export default ChallengeMultipleChoice;
