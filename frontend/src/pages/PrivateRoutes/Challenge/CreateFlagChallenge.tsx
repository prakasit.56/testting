import { Formik, Form } from 'formik'
import { useMutation } from 'urql'

import React, { FC } from 'react'

import * as form from "components/Form"
import { challengeAPI } from 'services/graphql';
import { CreateFlagChallengeDataInterface } from 'interfaces/pages/Challenge'
import * as validateSchemaForm from 'constants/validateSchemaForm'
import { GiCoinsPile } from 'react-icons/gi';

const CreateFlagChallenge: FC = () => {
    const [showFirstModal, setShowFirstModal] = React.useState(false);

    const [CreateFlagResult, CreateFlag] = useMutation(challengeAPI.mutation.createChallengeMutation)

    const handleSubmit = (data: CreateFlagChallengeDataInterface) => {
        challengeAPI.handler.handleOnSubmitCreateFlagChallenge(
            CreateFlag,
            data
        )
        console.log("CreateFlagResult")
        setShowFirstModal(false)
        console.log(CreateFlagResult)
    }

    const initialValues: CreateFlagChallengeDataInterface = {
        name: '',
        question: '',
        description: '',
        video_link: '',
        answer: '',
        hint: '',
    }

    return (
        <div className='form h-4/5 w-8/12 flex px-12 py-8 text-lg pb-16'>
            <div className='grid text-3xl'>
                <div>Create Challenge</div>
                <div className='text-[#0FB1D9]'>Flag</div>
            </div>
            <br/><br/>
            <Formik
                enableReinitialize
                initialValues={initialValues}
                onSubmit={handleSubmit}
                validationSchema={validateSchemaForm.CreatFlagChallengeValidationSchema()}
            >
                {({ errors, touched }) => (
                    <Form id="addFlagChallenge">
                        <div className='flex gap-10 justify-between'>
                            <div className='w-9/12 grid gap-y-4 text-left'>
                            {form.DataFieldForm.CreateFlagDataFieldForm().map((formData) => (
                                <form.CreateChallengeFieldForm
                                    key={formData.name}
                                    touched={touched[formData.name]}
                                    errors={errors[formData.name]}
                                    label={`${formData.label}`}
                                    name={formData.name}
                                    type={formData.type}
                                    placeholder={formData.placeholder}
                                />
                                ))}
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
            <br/>
        <div className='flex gap-x-5'>
            {/* modal */}
            <button onClick={() => window.location.href='/create-challenge'} className='w-28 bg-[#0062B9] py-2 hover:bg-white hover:text-[#0062B9]'>
                Back
            </button>
            <button
                className="w-32 bg-[#166615] py-2 hover:bg-white hover:text-[#166615]"
                type="button"
                onClick={() => setShowFirstModal(true)}
            >
                Submit
            </button>
            {showFirstModal ? (
                <>
                <div
                    className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                >
                    <div className="relative w-auto my-6 mx-auto max-w-3xl">
                    {/*content*/}
                    <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                        {/*header*/}
                        <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                        <h3 className="text-3xl font-semibold text-black">
                            Confirm
                        </h3>
                        <button
                            className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                            onClick={() => setShowFirstModal(false)}
                        >
                            ×
                        </button>
                        </div>
                        {/*body*/}
                        <div className="relative p-6 flex-auto">
                        <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                            Are you sure you want to send your challenge to admin  ?
                        </p>
                        </div>
                        {/*footer*/}
                        <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                            <button
                                className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="button"
                                onClick={() => setShowFirstModal(false)}
                            >
                                Back
                            </button>
                            <button
                                form="addFlagChallenge"
                                className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                type="submit"
                            >
                                Continue
                            </button>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                </>
            ) : null}
            </div>
            
        </div>
    
    )
}
export default CreateFlagChallenge
