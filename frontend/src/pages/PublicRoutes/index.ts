import HomePage from 'pages/PublicRoutes/Home/HomePage'
import SignUpPage from 'pages/PublicRoutes/Signup/SignupPage'
import SignInPage from 'pages/PublicRoutes/Signin/SigninPage'
import MainEventPage from 'pages/PublicRoutes/MainEvent/MainEventPage'

export {
    HomePage,
    SignUpPage,
    SignInPage,
    MainEventPage
}
