import { Formik, Form } from 'formik'
import { useMutation } from 'urql'

import { CircleLeft, CircleRight } from "components/Circle";
import { DotLeft, DotRight } from "components/Dot";
import * as form from "components/Form"

import { userAPI } from 'services/graphql';
import { SignUpInterface } from 'interfaces/pages/SignUp'
import * as validateSchemaForm from 'constants/validateSchemaForm'

const SignupPage = () => {

  const [SignUpResult, SignUp] = useMutation(userAPI.mutation.signUpMutation)

  const handleSubmit = (data: SignUpInterface) => {
    userAPI.handler.handleOnSubmitSignUp(
      SignUp,
      data,
    )

    console.log(SignUpResult)
  }

  const initialValues: SignUpInterface = {
    username: '',
    email: '',
    password: '',
  }

  return (
    <>
    <CircleLeft />
    <CircleRight />
      <div
        style={{ minHeight: "calc(100vh - (80px))" }}
        className="relative flex min-h-screen items-center justify-center overflow-hidden"
      >
        <div className="relative p-8">
          <DotLeft className="bottom-0" />
          <h2 className="mb-2 text-4xl font-bold text-white">Sign Up</h2>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            onSubmit={handleSubmit}
            validationSchema={validateSchemaForm.SignupValidationSchema()}
          >
            {({ errors, touched }) => (
              <Form>
                <div
                  className="flex w-96 flex-col gap-6 rounded-xl bg-secondary p-8"
                >
                  {form.DataFieldForm.signUpDataFieldForm().map((formData) => (
                    <form.DefaultForm
                      key={formData.name}
                      touched={touched[formData.name]}
                      errors={errors[formData.name]}
                      label={`${formData.label}`}
                      name={formData.name}
                      type={formData.type}
                      placeholder={formData.placeholder}
                    />
                  ))}
                  <div className="mt-2 flex w-full items-center justify-between">
                    <a href="/sign-in">
                      <u className="text-xs text-white text-opacity-50 hover:text-white">
                        <h6>
                          Already have an account?
                        </h6>
                      </u>
                    </a>
                    <button
                      type="submit"
                      className="rounded-xl w-24 bg-[#0062B9] py-3 px-4 text-3xl font-Roboto text-white shadow-md 
                                hover:bg-white hover:text-[#0062B9] transition-all duration-150 active:bg-[#3FA1F9] "
                    >
                      <h3 className="text-sm font-light leading-4 tracking-wide">
                        Continue
                      </h3>
                    </button>
                  </div>
                </div>
              </Form>
            )}
          </Formik>
          <DotRight className="top-0" />
        </div>
      </div>
    </>
  );
}

export default SignupPage;
