import { FeaturesSection, WelcomeSection } from "pages/PublicRoutes/Home/components";

const HomePage = () => {
  return (
    <>
      <WelcomeSection />
      <FeaturesSection />
    </>
  );
}
export default HomePage;
