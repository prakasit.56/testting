import React from 'react'
import { Formik, Form } from 'formik'

import { SignInInterface ,SignInPasswordInterface } from 'interfaces/pages/SignIn'
import * as validateSchemaForm from 'constants/validateSchemaForm'
import * as form from "components/Form"

type Props = {
  formTitle?: string;
  prevStep: () => void;
  nextStep: () => void;
  gotoStep: (step: number) => void;
  userData: SignInInterface
  setUserData: React.Dispatch<React.SetStateAction<SignInInterface>>
};

const PasswordStep =(props: Props) => {
  const { formTitle, prevStep, nextStep, userData, setUserData } = props;

  const initialValues: SignInPasswordInterface = {
    password: userData.password,
  }

  const onClickContinue = (data:SignInPasswordInterface) => {
    setUserData({username: userData.username, password: data.password})

    nextStep()
  }
  return (
    <>
      <h2 className="mb-2 text-4xl font-bold text-white">{formTitle}</h2>
        <Formik
          enableReinitialize
          initialValues={initialValues}
          onSubmit={onClickContinue}
          validationSchema={validateSchemaForm.SignInPasswordValidationSchema()}
        >
            {({ errors, touched }) => (
              <Form>
                 <div className="flex flex-col gap-1">
                  {form.DataFieldForm.signInDataFieldFormPassword().map((formData) => (
                    <form.DefaultForm
                      key={formData.name}
                      touched={touched[formData.name]}
                      errors={errors[formData.name]}
                      label={`${formData.label}`}
                      name={formData.name}
                      type={formData.type}
                      placeholder={formData.placeholder}
                    />
                  ))}
                </div>
                <div className="w-full">
                  <a href="/sig-up">
                    <u className="text-xs text-white text-opacity-50 hover:text-white hover:underline">
                      <h6>
                        Do not have an account?
                      </h6>
                    </u>
                  </a>
                </div>

                <div className="my-6 flex w-full items-center justify-center gap-3">
                  <button
                    className="h-2 w-2 rounded-full bg-gray-400"
                    // onClick={() => gotoStep(0)}
                  ></button>
                  <button
                    className="h-2 w-2 rounded-full bg-white"
                    // onClick={() => gotoStep(1)}
                  ></button>
                  <button
                    className="h-2 w-2 rounded-full bg-gray-400"
                    // onClick={() => gotoStep(2)}
                  ></button>
                </div>

                <div className="mt-8 flex w-full justify-start gap-4">
                  <button
                    type="button"
                    className={`w-24 rounded-xl bg-[#0062B9] py-3 px-4 text-3xl font-semibold text-white shadow-md`}
                    onClick={prevStep}
                  >
                    <h3 className="text-sm font-light leading-4 tracking-wide transition-all duration-150 active:bg-[#3FA1F9] ">
                      Back
                    </h3>
                  </button>
                  <button
                    type="submit"
                    className={`w-24 rounded-xl bg-[#0062B9] py-3 px-4 text-3xl font-semibold text-white shadow-md`}
                  >
                    <h3 className="text-sm font-light leading-4 tracking-wide transition-all duration-150 active:bg-[#3FA1F9] ">
                      Continue
                    </h3>
                  </button>
                </div>
              </Form>
            )}
      </Formik>
    </>
  );
}

export default PasswordStep;
