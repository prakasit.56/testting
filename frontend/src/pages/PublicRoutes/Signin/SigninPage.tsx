import { CircleLeft, CircleRight } from "components/Circle";
import { DotLeft, DotRight } from "components/Dot";
import { useMemo, useState } from "react";
import {
  PasswordStep,
  UsernameStep,
  StaySignedInStep,
} from "pages/PublicRoutes/Signin/components";
import { SignInInterface } from 'interfaces/pages/SignIn'



const SignupPage = () =>  {
  const [step, setStep] = useState(0);
  const [userData, setUserData] = useState<SignInInterface>({
    username: "",
    password: ""
  });

  const formTitles = useMemo(
    () => ["Sign In", "Sign In", "Stay signed in?"],
    []
  );

  const handleNextStep = () => {
    if (step === 2) {
      window.location.href = "/";
      return 
    }
    setStep((currentStep) => currentStep + 1);
  };

  const handlePrevStep = () => {
    if (step === 0) {
      window.location.href = "/";
      return 
    }
    
    setStep((currentStep) => currentStep - 1);
  };

  const gotoStep = (stepI: number) => {
    setStep(stepI);
  };

  const rederStep = () => {
    if(step > 2)
      return ''

    if(step === 0)
      return (
        <UsernameStep
          formTitle={formTitles[step]}
          gotoStep={gotoStep}
          prevStep={handlePrevStep}
          nextStep={handleNextStep}
          userData={userData}
          setUserData={setUserData}
        />
      )
    
    return step === 1 ? 
        <PasswordStep
          formTitle={formTitles[step]}
          gotoStep={gotoStep}
          userData={userData}
          setUserData={setUserData}
          prevStep={handlePrevStep}
          nextStep={handleNextStep}
        /> :
        <StaySignedInStep
          formTitle={formTitles[step]}
          gotoStep={gotoStep}
          userData={userData}
          prevStep={handlePrevStep}
        />
  }

  return (
    <>
      <div
        style={{ minHeight: "calc(100vh - (80px))"}}
        className="relative flex min-h-screen items-center justify-center overflow-hidden bg-primary"
      >
        <CircleLeft />
        <CircleRight />
        <div className="relative p-8">
          <DotLeft className="bottom-0" />
          <div className="flex w-96 flex-col gap-6 rounded-xl bg-secondary p-8 ">
            {rederStep()}
          </div>
          <DotRight className="top-0" />
        </div>
      </div>
    </>
  );
}

export default SignupPage;
