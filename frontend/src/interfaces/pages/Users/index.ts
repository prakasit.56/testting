export interface UserInterface {
    name: string,
    email: string,
    conuntry_code: string,
    github_link: string,
    twitter_link: string,
    facebook_link: string,
}

export interface UserScore {
    users: string,
    user_id: string,
    challenges: string,
    challenge_id: string,
    base_score: string,
    time_score: string,
}