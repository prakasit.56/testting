export interface ChallengeInterface {
    name: string,
    question: string,
    description: string,
    levels: string,
    level_id: string,
    types: string,
    types_id: string,
    categories: string,
    categories_id: string,
    award_badge: string,
    scorce: string,
    max_time: string,
    max_scorce: string,
}

export interface ChallengeDataInterface {
    name: string,
    question: string,
    description: string,
    scorce: string,
}

export interface CreateFlagChallengeDataInterface {
    name : string,
    question: string,
    description: string,
    video_link: string,
    answer: string,
    hint: string,
}

export interface CreateShortAnswerChallengeDataInterface {
    name : string,
    question: string,
    description: string,
    answer: string,
    hint: string,
}

export interface SubmitFlagChallengeDataInterface {
    user_id: string,
    challenge_id: string,
    base_score: string,
    time_score: string,
    level_id: string,
    types_id: string,
    categories_id: string,
    answer: string,
}