export interface SignUpInterface {
    username: string,
    email: string,
    password: string,
}